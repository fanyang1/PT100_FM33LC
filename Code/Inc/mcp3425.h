#ifndef __MCP3425_H
#define __MCP3425_H

#include "bsp.h"

extern uint8_t h_byte;
extern uint8_t l_byte;
extern uint8_t c_byte;

uint8_t mcp3425_init(void);
uint8_t get_ad(void);
uint32_t get_vout(void);

#endif
