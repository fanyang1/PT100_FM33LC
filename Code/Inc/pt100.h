#ifndef __MPT100_H
#define __PT100_H

#include "bsp.h"

#define  NTC_TABLE_COUNT 2030

extern float  RTD_TAB_PT100[NTC_TABLE_COUNT];

float NTC_ADCConv(void);
uint16_t NTC_SearchTable(float adc_val);
bool NTC_GetT(int16_t *t_z, uint8_t *t_f);


#endif

