#include "bsp.h"

uint8_t h_byte = 0;
uint8_t l_byte = 0;
uint8_t c_byte = 0;

//返回值 1 2 3：初始化失败  0：初始化成功
uint8_t mcp3425_init(void)
{
    uint8_t result;

    //-------------- start bit -------------
	result = I2C_Send_Bit( STARTBIT );//发送起始位
	if( result != 0 ) 
        return 1; //failure.
	//-------------- disable read -------------
	LL_I2C_MasterMode_DisableReceive(I2C);
	//-------------- device addr -------------
	result = I2C_Send_Byte( 0xD0 ); //发送器件地址
	if( result != 0 ) 
        return 2; //failure.
    result = I2C_Send_Byte( 0x19 ); //发送数据地址高8位
	if( result != 0 ) 
        return 3; //failure.
    I2C_Send_Bit( STOPBIT ); //发送停止位

    return 1; //success.
}


uint8_t get_ad(void)
{
     uint8_t result;
    //-------------- start bit -------------
	result = I2C_Send_Bit( STARTBIT );//发送起始位
	if( result != 0 ) 
        return 1; //failure.
    result = I2C_Send_Byte( 0xD1 ); //发送器件地址
	if( result != 0 ) 
        return 2; //failure.

    LL_I2C_MasterMode_SetMasterRespond(I2C,LL_I2C_SSP_MASTER_RESPOND_ACK);
    I2C_Receive_Byte( &h_byte );    

    LL_I2C_MasterMode_SetMasterRespond(I2C,LL_I2C_SSP_MASTER_RESPOND_ACK);
    I2C_Receive_Byte( &l_byte ); 

    LL_I2C_MasterMode_SetMasterRespond(I2C,LL_I2C_SSP_MASTER_RESPOND_NACK);
    I2C_Receive_Byte( &c_byte );    

    if( I2C_Send_Bit( STOPBIT ) ) //发送停止位
         return 3; //failure.
    return 0; //success.
}

uint32_t get_vout(void) //获取实际电压值 uV
{
    uint32_t result;

    get_ad();

    result = ((h_byte * 256 + l_byte )*625 )/20; //uv

    return (result);
}


