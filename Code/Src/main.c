#include "bsp.h"

uint8_t i = 0;
int16_t t_z;
uint8_t t_f;
float NTC_ADCCon[10],databuffer[10];
float temp;
uint8_t temp_buf[4] = {0,0,0,0xFE}; //USB数据发送电脑缓存，最后一个字节作为帧尾判断

int main(void)
{
    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    /* SHOULD BE KEPT!!! */
    MF_Clock_Init();
    ClockInit();
    SystickInit();
	
//    Test_RTC();
    bsp_Init();

    USBInit();

    mcp3425_init(); //连续转换模式  16位精度，实际使用低15位，最高位代表符号位

//    DelayMs(2000);
    show_bat();


    while(1)
    {           
        if(is1hz == 1)
        {
            show_bat();
        }

        if(is1min == 1)
        {
            is1min = 0;        
        }
        
        if(is8hz == 1)
        {
            NTC_ADCCon[i++] = NTC_ADCConv();
            if(i > 4)
            {
                i = 0;
                LL_RTC_DisableIT_8hz(RTC);

                for(uint8_t t=0;t<4;t++)
                    databuffer[t] = NTC_ADCCon[t];

                for(uint8_t t=0;t<4;t++)
                {
                    if(databuffer[t] > databuffer[t+1])//升序排列
                    {
                        temp=databuffer[t+1];
                        databuffer[t+1] = databuffer[t];
                        databuffer[t] = temp;
                    }  
                }
                LL_RTC_EnableIT_8hz(RTC);
            }
            is8hz = 0;
        }

        if(is2hz == 1)
        {   
            NTC_GetT(&t_z,&t_f);
            LCD_Show_Count(t_z*10 + t_f);

            temp_buf[0] = t_z/256 ;  //温度整数部分
            temp_buf[1] = t_z%256 ;
            temp_buf[2] = t_f;      //温度小数部分
            StreamWriteData(&USBTxStream, temp_buf, 4);//usb发送

            SET_VALUE_DP9;SET_VALUE_S9;
            LCD_EndDraw();
            is2hz = 0;
        }
    }
}


