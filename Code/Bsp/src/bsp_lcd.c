#include "bsp.h"
#include "Lcdzhen.h"



////=====================================优化LCD显示代码==================================================//
uint32_t DISPDATA[10];     //定义数据寄存器
uint32_t s_DispBuf[10];    //定义数据缓冲区 

uint8_t  s_ucUpdateEn = 1; /* 为了优化显示逻辑部分，引入刷屏标志。0 表示显示函数只改写缓冲区，不写屏。1 表示直接写屏 */
uint32_t Count32 = 0;      //段码屏显示的数字

/*
*********************************************************************************************************
*	函 数 名: LCD_ShowALL
*	功能说明: LCD全显示
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void LCD_ShowALL(void)
{
    LL_LCD_SetDisplayData(LCD, 0xFFFFFFFF, LL_LCD_DATA_REG_0);
    LL_LCD_SetDisplayData(LCD, 0xFFFFFFFF, LL_LCD_DATA_REG_1);
    LL_LCD_SetDisplayData(LCD, 0xFFFFFFFF, LL_LCD_DATA_REG_2);
    LL_LCD_SetDisplayData(LCD, 0xFFFFFFFF, LL_LCD_DATA_REG_3);
}

/*
*********************************************************************************************************
*	函 数 名: LCD_ShowNO
*	功能说明: LCD全熄灭
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void LCD_ShowNO(void)
{
    LL_LCD_SetDisplayData(LCD, 0x00000000, LL_LCD_DATA_REG_0);
    LL_LCD_SetDisplayData(LCD, 0x00000000, LL_LCD_DATA_REG_1);
    LL_LCD_SetDisplayData(LCD, 0x00000000, LL_LCD_DATA_REG_2);
    LL_LCD_SetDisplayData(LCD, 0x00000000, LL_LCD_DATA_REG_3);
}

/*
*********************************************************************************************************
*	函 数 名: LCD_StartDraw
*	功能说明: 开始绘图。以后绘图函数只改写缓冲区，不改写面板显存
*	形    参:  无
*	返 回 值:  无
*********************************************************************************************************
*/
void LCD_StartDraw(void)
{
	s_ucUpdateEn = 0;
}

/*
*********************************************************************************************************
*	函 数 名: LCD_EndDraw
*	功能说明: 结束绘图。缓冲区的数据刷新到面板显存。 LCD_StartDraw() 和 LCD_EndDraw() 必须成对使用
*	形    参:  无
*	返 回 值:  无
*********************************************************************************************************
*/
void LCD_EndDraw(void)
{
	s_ucUpdateEn = 1;
	LCD_BufToPanel();
}

/*
*********************************************************************************************************
*	函 数 名: LCD_BufToPanel
*	功能说明: 将缓冲区中的数据写入LCD面板
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void LCD_BufToPanel(void)
{
    unsigned char i;

    if(s_ucUpdateEn == 1)
    {
        for(i=0;i<4;i++)
            s_DispBuf[i] = DISPDATA[i];       //更新缓存数据

        LL_LCD_SetDisplayData(LCD, s_DispBuf[0], LL_LCD_DATA_REG_0);//刷新显示
        LL_LCD_SetDisplayData(LCD, s_DispBuf[1], LL_LCD_DATA_REG_1);
        LL_LCD_SetDisplayData(LCD, s_DispBuf[2], LL_LCD_DATA_REG_2);
        LL_LCD_SetDisplayData(LCD, s_DispBuf[3], LL_LCD_DATA_REG_3);
  } 
}

//=================================以下部分为显示纯数字代码==============================================//
                                    //数码管显示定义
                                    /*  /---A---/
                                       /F     B/
                                      /---G---/   ABCDEFGH ---- [7-0] A最高位 H最低位
                                     /E     C/
                                    /---D---/ P
                                    */
///*
//*********************************************************************************************************
//*	函 数 名: LCD_Table
//*	功能说明:  将缓冲区中的数据写入LCD面板
//*	形    参:  Show :显示的单个字符
//*	返 回 值: 显示的字符对应的数码管段码
//*********************************************************************************************************
//*/
unsigned char  LCD_Table( unsigned char  Show )
{
	const unsigned char code_value[] = 
//	{ 0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, 0x7f, 
//	  0x6f, 0x50, 0x40, 0x38, 0x00, 0x79, 0x71, 0x76, 0x38 };
	{0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, 0x7f,0x6f,//0~9
//	 0x50, 0x40, 0x38, 0x76, 0x79, 0x71};//r,-,L,H,E,F
//	0x77, 0x7c, 0x39, 0x5e, 0x79, 0x71};//A,B,C,D,E,F
	0x77, 0x7c, 0x39, 0x5e, 0x79, 0x00};//A,B,C,D,E,空	 
	if( Show < 0x12 )
	{
		return ( code_value[Show] );
	}
	else
	{
		return (0x3f);
	}
}


/*
*********************************************************************************************************
*	函 数 名: LCD_Num_Data
*	功能说明:  每一位数码管显示单独的数字 （无小数点）        
*	形    参:  Num ：选中某一个数码管
                  Show : 显示的单个字符
*	返 回 值: 无
*********************************************************************************************************
*/
void LCD_Num_Data( uint08 Num, uint08 Show)
{
	unsigned char Result;

	Result = LCD_Table( Show & 0x0f );
    
	switch( Num )   //显示的数
	{
//		case 0x01:
//		//------------ 第1个"8"字符 ----------
//        if( (Result&0x01) == 0x01 ) {	SET_VALUE_1A;}	else {  RESET_VALUE_1A;}//1A
//		if( (Result&0x02) == 0x02 ) {	SET_VALUE_1B;}	else {  RESET_VALUE_1B;}//1B
//		if( (Result&0x04) == 0x04 ) {	SET_VALUE_1C;}	else {  RESET_VALUE_1C;}//1C
//		if( (Result&0x08) == 0x08 ) {   SET_VALUE_1D;}	else {  RESET_VALUE_1D;}//1D
//		if( (Result&0x10) == 0x10 ) {	SET_VALUE_1E;}	else {  RESET_VALUE_1E;}//1E
//		if( (Result&0x20) == 0x20 ) {	SET_VALUE_1F;}	else {  RESET_VALUE_1F;}//1F
//		if( (Result&0x40) == 0x40 ) {	SET_VALUE_1G;}	else {  RESET_VALUE_1G;}//1G
//		break;

//		//------------ 第2个"8"字符 ----------
//		case 0x02:
//		if( (Result&0x01) == 0x01 ) {	SET_VALUE_2A;}	else {  RESET_VALUE_2A;}//2A	  
//		if( (Result&0x02) == 0x02 ) {	SET_VALUE_2B;}	else {  RESET_VALUE_2B;}//2B	  
//		if( (Result&0x04) == 0x04 ) {	SET_VALUE_2C;}	else {  RESET_VALUE_2C;}//2C		  
//		if( (Result&0x08) == 0x08 ) {	SET_VALUE_2D;}	else {  RESET_VALUE_2D;}//2D		  
//		if( (Result&0x10) == 0x10 ) {	SET_VALUE_2E;}	else {  RESET_VALUE_2E;}//2E	  
//		if( (Result&0x20) == 0x20 ) {	SET_VALUE_2F;}	else {  RESET_VALUE_2F;}//2F		  
//		if( (Result&0x40) == 0x40 ) {	SET_VALUE_2G;}	else {  RESET_VALUE_2G;}//2G			  
//		break;
//	
//		//------------ 第3个"8"字符 ----------
//		case 0x03:
//		if( (Result&0x01) == 0x01 ) {	SET_VALUE_3A;}	else {  RESET_VALUE_3A;}//3A	  
//		if( (Result&0x02) == 0x02 ) {	SET_VALUE_3B;}	else {  RESET_VALUE_3B;}//3B	  
//		if( (Result&0x04) == 0x04 ) {	SET_VALUE_3C;}	else {  RESET_VALUE_3C;}//3C		  
//		if( (Result&0x08) == 0x08 ) {	SET_VALUE_3D;}	else {  RESET_VALUE_3D;}//3D		  
//		if( (Result&0x10) == 0x10 ) {	SET_VALUE_3E;}	else {  RESET_VALUE_3E;}//3E	  
//		if( (Result&0x20) == 0x20 ) {	SET_VALUE_3F;}	else {  RESET_VALUE_3F;}//3F		  
//		if( (Result&0x40) == 0x40 ) {	SET_VALUE_3G;}	else {  RESET_VALUE_3G;}//3G	
//		break;	
//		
//		//------------ 第4个"8"字符 ----------
//		case 0x04:
//		if( (Result&0x01) == 0x01 ) {	SET_VALUE_4A;}	else {  RESET_VALUE_4A;}//4A	  
//		if( (Result&0x02) == 0x02 ) {	SET_VALUE_4B;}	else {  RESET_VALUE_4B;}//4B	  
//		if( (Result&0x04) == 0x04 ) {	SET_VALUE_4C;}	else {  RESET_VALUE_4C;}//4C		  
//		if( (Result&0x08) == 0x08 ) {	SET_VALUE_4D;}	else {  RESET_VALUE_4D;}//4D		  
//		if( (Result&0x10) == 0x10 ) {	SET_VALUE_4E;}	else {  RESET_VALUE_4E;}//4E	  
//		if( (Result&0x20) == 0x20 ) {	SET_VALUE_4F;}	else {  RESET_VALUE_4F;}//4F		  
//		if( (Result&0x40) == 0x40 ) {	SET_VALUE_4G;}	else {  RESET_VALUE_4G;}//4G
//		break;
//		
		case 0x05:
		//------------ 第1个"8"字符 ----------
        if( (Result&0x01) == 0x01 ) {	SET_VALUE_5A;}	else {  RESET_VALUE_5A;}//1A
		if( (Result&0x02) == 0x02 ) {	SET_VALUE_5B;}	else {  RESET_VALUE_5B;}//1B
		if( (Result&0x04) == 0x04 ) {	SET_VALUE_5C;}	else {  RESET_VALUE_5C;}//1C
		if( (Result&0x08) == 0x08 ) {   SET_VALUE_5D;}	else {  RESET_VALUE_5D;}//1D
		if( (Result&0x10) == 0x10 ) {	SET_VALUE_5E;}	else {  RESET_VALUE_5E;}//1E
		if( (Result&0x20) == 0x20 ) {	SET_VALUE_5F;}	else {  RESET_VALUE_5F;}//1F
		if( (Result&0x40) == 0x40 ) {	SET_VALUE_5G;}	else {  RESET_VALUE_5G;}//1G
		break;

		//------------ 第2个"8"字符 ----------
		case 0x06:
		if( (Result&0x01) == 0x01 ) {	SET_VALUE_6A;}	else {  RESET_VALUE_6A;}//2A	  
		if( (Result&0x02) == 0x02 ) {	SET_VALUE_6B;}	else {  RESET_VALUE_6B;}//2B	  
		if( (Result&0x04) == 0x04 ) {	SET_VALUE_6C;}	else {  RESET_VALUE_6C;}//2C		  
		if( (Result&0x08) == 0x08 ) {	SET_VALUE_6D;}	else {  RESET_VALUE_6D;}//2D		  
		if( (Result&0x10) == 0x10 ) {	SET_VALUE_6E;}	else {  RESET_VALUE_6E;}//2E	  
		if( (Result&0x20) == 0x20 ) {	SET_VALUE_6F;}	else {  RESET_VALUE_6F;}//2F		  
		if( (Result&0x40) == 0x40 ) {	SET_VALUE_6G;}	else {  RESET_VALUE_6G;}//2G			  
		break;
	
		//------------ 第3个"8"字符 ----------
		case 0x07:
		if( (Result&0x01) == 0x01 ) {	SET_VALUE_7A;}	else {  RESET_VALUE_7A;}//3A	  
		if( (Result&0x02) == 0x02 ) {	SET_VALUE_7B;}	else {  RESET_VALUE_7B;}//3B	  
		if( (Result&0x04) == 0x04 ) {	SET_VALUE_7C;}	else {  RESET_VALUE_7C;}//3C		  
		if( (Result&0x08) == 0x08 ) {	SET_VALUE_7D;}	else {  RESET_VALUE_7D;}//3D		  
		if( (Result&0x10) == 0x10 ) {	SET_VALUE_7E;}	else {  RESET_VALUE_7E;}//3E	  
		if( (Result&0x20) == 0x20 ) {	SET_VALUE_7F;}	else {  RESET_VALUE_7F;}//3F		  
		if( (Result&0x40) == 0x40 ) {	SET_VALUE_7G;}	else {  RESET_VALUE_7G;}//3G	
		break;	
		
		//------------ 第4个"8"字符 ----------
		case 0x08:
		if( (Result&0x01) == 0x01 ) {	SET_VALUE_8A;}	else {  RESET_VALUE_8A;}//4A	  
		if( (Result&0x02) == 0x02 ) {	SET_VALUE_8B;}	else {  RESET_VALUE_8B;}//4B	  
		if( (Result&0x04) == 0x04 ) {	SET_VALUE_8C;}	else {  RESET_VALUE_8C;}//4C		  
		if( (Result&0x08) == 0x08 ) {	SET_VALUE_8D;}	else {  RESET_VALUE_8D;}//4D		  
		if( (Result&0x10) == 0x10 ) {	SET_VALUE_8E;}	else {  RESET_VALUE_8E;}//4E	  
		if( (Result&0x20) == 0x20 ) {	SET_VALUE_8F;}	else {  RESET_VALUE_8F;}//4F		  
		if( (Result&0x40) == 0x40 ) {	SET_VALUE_8G;}	else {  RESET_VALUE_8G;}//4G
		break;
		
		//------------ 第5个"8"字符 ----------
		case 0x09:
		if( (Result&0x01) == 0x01 ) {	SET_VALUE_9A;}	else {  RESET_VALUE_9A;}//4A	  
		if( (Result&0x02) == 0x02 ) {	SET_VALUE_9B;}	else {  RESET_VALUE_9B;}//4B	  
		if( (Result&0x04) == 0x04 ) {	SET_VALUE_9C;}	else {  RESET_VALUE_9C;}//4C		  
		if( (Result&0x08) == 0x08 ) {	SET_VALUE_9D;}	else {  RESET_VALUE_9D;}//4D		  
		if( (Result&0x10) == 0x10 ) {	SET_VALUE_9E;}	else {  RESET_VALUE_9E;}//4E	  
		if( (Result&0x20) == 0x20 ) {	SET_VALUE_9F;}	else {  RESET_VALUE_9F;}//4F		  
		if( (Result&0x40) == 0x40 ) {	SET_VALUE_9G;}	else {  RESET_VALUE_9G;}//4G
		break;
		
		//------------ 第6个"8"字符 ----------
		case 0x0a:
		if( (Result&0x01) == 0x01 ) {	SET_VALUE_10A;}	else {  RESET_VALUE_10A;}//4A	  
		if( (Result&0x02) == 0x02 ) {	SET_VALUE_10B;}	else {  RESET_VALUE_10B;}//4B	  
		if( (Result&0x04) == 0x04 ) {	SET_VALUE_10C;}	else {  RESET_VALUE_10C;}//4C		  
		if( (Result&0x08) == 0x08 ) {	SET_VALUE_10D;}	else {  RESET_VALUE_10D;}//4D		  
		if( (Result&0x10) == 0x10 ) {	SET_VALUE_10E;}	else {  RESET_VALUE_10E;}//4E	  
		if( (Result&0x20) == 0x20 ) {	SET_VALUE_10F;}	else {  RESET_VALUE_10F;}//4F		  
		if( (Result&0x40) == 0x40 ) {	SET_VALUE_10G;}	else {  RESET_VALUE_10G;}//4G
		break;
		
	default:
		break;
	}
}

/*
*********************************************************************************************************
*	函 数 名: GetLengthNum
*	功能说明:  判断输入的数据是几位数   
*	形    参:  Num ：需要进行判断的数
*	返 回 值: 数据长度
*********************************************************************************************************
*/
unsigned char GetLengthNum(uint32 Num)
{
   uint32  n = 0;

   for (n = 0; Num > 0; n++) 
   { 
     Num /= 10; 
   }
   return n;
}

/*
*********************************************************************************************************
*	函 数 名: LCD_Show_Count
*	功能说明:  将需要的数据直接显示在段码屏上       
*	形    参:  ShowData ：需要显示的数
*	返 回 值: 无
*********************************************************************************************************
*/
void LCD_Show_Count(uint32 ShowData)
{
	uint08 Temp08;
	uint32 Temp32;
	u8 len;
        if(ShowData > 999999)
          ShowData = 999999;
        
	Temp32 = ShowData;
  len = GetLengthNum(ShowData); //获取数据长度
//  len = 6;
				
	switch(len)
        {
          case 0:
						LCD_Num_Data( 10, 0);
            break;
            
          case 1:
					
            Temp08 = Temp32;				
            LCD_Num_Data( 10, Temp08);
            break;
            
          case 2:
            Temp08 = Temp32/10;
            Temp32 = Temp32%10;					
            LCD_Num_Data( 9, Temp08);
	
            Temp08 = Temp32;				
            LCD_Num_Data( 10, Temp08);
            break;
            
          case 3:					
            Temp08 = Temp32/100;
            Temp32 = Temp32%100;					
            LCD_Num_Data( 8, Temp08);
	
            Temp08 = Temp32/10;
            Temp32 = Temp32%10;					
            LCD_Num_Data( 9, Temp08);
	
            Temp08 = Temp32;				
            LCD_Num_Data( 10, Temp08);
            break;
            
          case 4:
            Temp08 = Temp32/1000;
            Temp32 = Temp32%1000;					
            LCD_Num_Data( 7, Temp08);
	
            Temp08 = Temp32/100;
            Temp32 = Temp32%100;					
            LCD_Num_Data( 8, Temp08);
	
            Temp08 = Temp32/10;
            Temp32 = Temp32%10;					
            LCD_Num_Data( 9, Temp08);
	
            Temp08 = Temp32;				
            LCD_Num_Data( 10, Temp08);
            break;
        
			  case 5:
			  Temp08 = Temp32/10000;
            Temp32 = Temp32%10000;							
            LCD_Num_Data( 6, Temp08);
					
            Temp08 = Temp32/1000;
            Temp32 = Temp32%1000;					
            LCD_Num_Data( 7, Temp08);
	
            Temp08 = Temp32/100;
            Temp32 = Temp32%100;					
            LCD_Num_Data( 8, Temp08);
	
            Temp08 = Temp32/10;
            Temp32 = Temp32%10;					
            LCD_Num_Data( 9, Temp08);
	
            Temp08 = Temp32;				
            LCD_Num_Data( 10, Temp08);
            break;
					
			case 6:
			  Temp08 = Temp32/100000;
            Temp32 = Temp32%100000;							
            LCD_Num_Data( 5, Temp08);
					
						Temp08 = Temp32/10000;
            Temp32 = Temp32%10000;							
            LCD_Num_Data( 6, Temp08);
					
            Temp08 = Temp32/1000;
            Temp32 = Temp32%1000;					
            LCD_Num_Data( 7, Temp08);
	
            Temp08 = Temp32/100;
            Temp32 = Temp32%100;					
            LCD_Num_Data( 8, Temp08);
	
            Temp08 = Temp32/10;
            Temp32 = Temp32%10;					
            LCD_Num_Data( 9, Temp08);
	
            Temp08 = Temp32;				
            LCD_Num_Data( 10, Temp08);
            break;
															
          default:
            break;
        }
}     

///*
//*********************************************************************************************************
//*	函 数 名: LCD_Show_Time
//*	功能说明:  将需要的时间直接显示在段码屏上       
//*	形    参:  Time ：需要显示的时间
//*	返 回 值: 无
//*********************************************************************************************************
//*/
//void LCD_Show_Time(void)
//{
//	uint08 Temp08;
//	RTC_TimeDateTypeDef temptime;
//		
//	RTC_GetRTC(&temptime);
//	      
//	Temp08 = temptime.Hour/10;				
//	LCD_Num_Data( 1, Temp08);

//	Temp08 = temptime.Hour%10;
//				
//	LCD_Num_Data( 2, Temp08);

//	Temp08 = temptime.Minute/10;			
//	LCD_Num_Data( 3, Temp08);

//	Temp08 = temptime.Minute%10;				
//	LCD_Num_Data( 4, Temp08);
//	
//  LCD_EndDraw();
//} 
void LcdInit(void)
{
    LL_GPIO_InitTypeDef GPIO_InitStruction;
    LL_LCD_InitTypeDef LCD_InitStruction;
    
    // GPIO Init
    GPIO_InitStruction.Pin = LL_GPIO_PIN_0 | LL_GPIO_PIN_1 | LL_GPIO_PIN_2 | 
    LL_GPIO_PIN_3 | LL_GPIO_PIN_4 | LL_GPIO_PIN_5 | LL_GPIO_PIN_6| LL_GPIO_PIN_7| 
    LL_GPIO_PIN_8 | LL_GPIO_PIN_9 | LL_GPIO_PIN_10;
    GPIO_InitStruction.Mode = LL_GPIO_MODE_ANALOG;
    GPIO_InitStruction.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruction.Pull = DISABLE;
    LL_GPIO_Init(GPIOA, &GPIO_InitStruction);
    
    GPIO_InitStruction.Pin =  LL_GPIO_PIN_4 |LL_GPIO_PIN_5 | LL_GPIO_PIN_6 |LL_GPIO_PIN_7 | 
    LL_GPIO_PIN_8 | LL_GPIO_PIN_9 | LL_GPIO_PIN_10 | LL_GPIO_PIN_11 
    |LL_GPIO_PIN_13 | LL_GPIO_PIN_14;
    GPIO_InitStruction.Mode = LL_GPIO_MODE_ANALOG;
    LL_GPIO_Init(GPIOB, &GPIO_InitStruction);
    
    GPIO_InitStruction.Pin = LL_GPIO_PIN_4 | LL_GPIO_PIN_5 ;
    GPIO_InitStruction.Mode = LL_GPIO_MODE_ANALOG;
    LL_GPIO_Init(GPIOC, &GPIO_InitStruction);

    GPIO_InitStruction.Pin = LL_GPIO_PIN_0 | LL_GPIO_PIN_1 | LL_GPIO_PIN_2 |
    LL_GPIO_PIN_3 | LL_GPIO_PIN_4 | LL_GPIO_PIN_5;
    GPIO_InitStruction.Mode = LL_GPIO_MODE_ANALOG;
    LL_GPIO_Init(GPIOD, &GPIO_InitStruction);
    
    // LCD Init
    LCD_InitStruction.BiasMode = LL_LCD_SET_BIASMD_DIV3BIAS;
    LCD_InitStruction.BiasVoltage = LL_LCD_GRAY_SCALE_CTRL_LEVEL_14;
    LCD_InitStruction.COMxNum    = LL_LCD_SEG_LINE_SELECT_COM_4;
    LCD_InitStruction.EnableMode = LL_LCD_DRV_ENMODE_INNERRESISTER;
    LCD_InitStruction.FreqPsc = 64;
    LCD_InitStruction.ICCtrl = LL_LCD_DRV_IC_CTRL_LARGE;
    LCD_InitStruction.Waveform = LL_LCD_WAVEFORM_FORMAT_TYPE_A;
    LL_LCD_Init(LCD, &LCD_InitStruction);
    
    // COM and SEG Init
    LL_LCD_EnableComPin(LCD, LL_LCD_COM_EN_COM0);
    LL_LCD_EnableComPin(LCD, LL_LCD_COM_EN_COM1);
    LL_LCD_EnableComPin(LCD, LL_LCD_COM_EN_COM2);
    LL_LCD_EnableComPin(LCD, LL_LCD_COM_EN_COM3);

    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG0);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG1);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG2);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG3);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG4);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG5);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG6);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG7);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG8);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG9);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG10);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG11);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG12);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG13);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG14);
//    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG15);
//    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG16);
//    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG17);
//    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG18);
//    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG19);
//    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG20);
//    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG21);

    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG22);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG23);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG24);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG25);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG26);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG27);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG28);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG29);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG30);
    LL_LCD_EnableSegPin(LCD, LL_LCD_SEG_EN0_SEG31);
      
    LL_LCD_Enable(LCD);
}

////0~9, A,B, C, D, E, F, 空
//const uint8_t FONT_CODE_TABLE[] = { 
//    0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, 0x7f, 0x6f,
//    0x77, 0x7c, 0x39, 0x5e, 0x79, 0x71, 0x00}; 

//uint32_t dispBuf[8] = {0};

//void LcdDisplayRefresh(void)
//{
//    LL_LCD_SetDisplayData(LCD, dispBuf[0], LL_LCD_DATA_REG_0);
//    LL_LCD_SetDisplayData(LCD, dispBuf[1], LL_LCD_DATA_REG_1);
//    LL_LCD_SetDisplayData(LCD, dispBuf[2], LL_LCD_DATA_REG_2);
//    LL_LCD_SetDisplayData(LCD, dispBuf[3], LL_LCD_DATA_REG_3);
//}

// true: 点亮; false: 熄灭
//void LcdDisplayBIT(uint32_t x, uint32_t y, bool value)
//{    
//    if (value == true)
//    {
//        LCD_SET_BIT(dispBuf[x - 1], y - 1);
//    }
//    else
//    {
//        LCD_RST_BIT(dispBuf[x - 1], y - 1);
//    }
//    LcdDisplayRefresh();
//}

//void LcdDisplayNumber(uint32_t number)
//{      
//    uint8_t digit = 8;
//    uint8_t font;
//        
//    while (digit)
//    {
//        font = FONT_CODE_TABLE[number % 10];
//        number /= 10;
//        
//        switch (digit--)
//        {
//        case 0x08:
//            //------------ 第1个"8"字符 ----------
//            if ((font & 0x01))  LCD_SET_BIT(dispBuf[1], 10);	//A
//            else                LCD_RST_BIT(dispBuf[1], 10);
//            if ((font & 0x02))  LCD_SET_BIT(dispBuf[2], 10);	//B
//            else                LCD_RST_BIT(dispBuf[2], 10);
//            if ((font & 0x04))  LCD_SET_BIT(dispBuf[3], 10);	//C
//            else                LCD_RST_BIT(dispBuf[3], 10);
//            if ((font & 0x08))  LCD_SET_BIT(dispBuf[4], 9);	    //D
//            else                LCD_RST_BIT(dispBuf[4], 9);
//            if ((font & 0x10))  LCD_SET_BIT(dispBuf[3], 9);	    //E
//            else                LCD_RST_BIT(dispBuf[3], 9);
//            if ((font & 0x20))  LCD_SET_BIT(dispBuf[1], 9);	    //F
//            else                LCD_RST_BIT(dispBuf[1], 9);
//            if ((font & 0x40))  LCD_SET_BIT(dispBuf[2], 9);	    //G
//            else                LCD_RST_BIT(dispBuf[2], 9);
//            break;

//        case 0x07:
//            //------------ 第2个"8"字符 ----------
//            if ((font & 0x01))  LCD_SET_BIT(dispBuf[1], 8);	    //A
//            else                LCD_RST_BIT(dispBuf[1], 8);
//            if ((font & 0x02))  LCD_SET_BIT(dispBuf[2], 8);	    //B
//            else                LCD_RST_BIT(dispBuf[2], 8);
//            if ((font & 0x04))  LCD_SET_BIT(dispBuf[3], 8);	    //C
//            else                LCD_RST_BIT(dispBuf[3], 8);
//            if ((font & 0x08))  LCD_SET_BIT(dispBuf[4], 7);	    //D
//            else                LCD_RST_BIT(dispBuf[4], 7);
//            if ((font & 0x10))  LCD_SET_BIT(dispBuf[3], 7);	    //E
//            else                LCD_RST_BIT(dispBuf[3], 7);
//            if ((font & 0x20))  LCD_SET_BIT(dispBuf[1], 7);	    //F
//            else                LCD_RST_BIT(dispBuf[1], 7);
//            if ((font & 0x40))  LCD_SET_BIT(dispBuf[2], 7);	    //G
//            else                LCD_RST_BIT(dispBuf[2], 7);		  
//            break;
//    
//        case 0x06:
//            //------------ 第3个"8"字符 ----------
//            if ((font & 0x01))  LCD_SET_BIT(dispBuf[1], 6);	    //A
//            else                LCD_RST_BIT(dispBuf[1], 6);
//            if ((font & 0x02))  LCD_SET_BIT(dispBuf[2], 6);	    //B
//            else                LCD_RST_BIT(dispBuf[2], 6);
//            if ((font & 0x04))  LCD_SET_BIT(dispBuf[3], 6);	    //C
//            else                LCD_RST_BIT(dispBuf[3], 6);
//            if ((font & 0x08))  LCD_SET_BIT(dispBuf[4], 5);	    //D
//            else                LCD_RST_BIT(dispBuf[4], 5);
//            if ((font & 0x10))  LCD_SET_BIT(dispBuf[3], 5);	    //E
//            else                LCD_RST_BIT(dispBuf[3], 5);
//            if ((font & 0x20))  LCD_SET_BIT(dispBuf[1], 5);	    //F
//            else                LCD_RST_BIT(dispBuf[1], 5);
//            if ((font & 0x40))  LCD_SET_BIT(dispBuf[2], 5);	    //G
//            else                LCD_RST_BIT(dispBuf[2], 5);	
//            break;	
//        
//        case 0x05:
//            //------------ 第4个"8"字符 ----------
//            if ((font & 0x01))  LCD_SET_BIT(dispBuf[1], 13);	//A
//            else                LCD_RST_BIT(dispBuf[1], 13);
//            if ((font & 0x02))  LCD_SET_BIT(dispBuf[2], 13);	//B
//            else                LCD_RST_BIT(dispBuf[2], 13);
//            if ((font & 0x04))  LCD_SET_BIT(dispBuf[3], 13);	//C
//            else                LCD_RST_BIT(dispBuf[3], 13);
//            if ((font & 0x08))  LCD_SET_BIT(dispBuf[4], 14);	//D
//            else                LCD_RST_BIT(dispBuf[4], 14);
//            if ((font & 0x10))  LCD_SET_BIT(dispBuf[3], 14);	//E
//            else                LCD_RST_BIT(dispBuf[3], 14);
//            if ((font & 0x20))  LCD_SET_BIT(dispBuf[1], 14);	//F
//            else                LCD_RST_BIT(dispBuf[1], 14);
//            if ((font & 0x40))  LCD_SET_BIT(dispBuf[2], 14);	//G
//            else                LCD_RST_BIT(dispBuf[2], 14);
//            break;	

//        case 0x04:
//            //------------ 第5个"8"字符 ----------
//            if ((font & 0x01))  LCD_SET_BIT(dispBuf[1], 15);	//A
//            else                LCD_RST_BIT(dispBuf[1], 15);
//            if ((font & 0x02))  LCD_SET_BIT(dispBuf[2], 15);	//B
//            else                LCD_RST_BIT(dispBuf[2], 15);
//            if ((font & 0x04))  LCD_SET_BIT(dispBuf[3], 15);	//C
//            else                LCD_RST_BIT(dispBuf[3], 15);
//            if ((font & 0x08))  LCD_SET_BIT(dispBuf[4], 16);	//D
//            else                LCD_RST_BIT(dispBuf[4], 16);
//            if ((font & 0x10))  LCD_SET_BIT(dispBuf[3], 16);	//E
//            else                LCD_RST_BIT(dispBuf[3], 16);
//            if ((font & 0x20))  LCD_SET_BIT(dispBuf[1], 16);	//F
//            else                LCD_RST_BIT(dispBuf[1], 16);
//            if ((font & 0x40))  LCD_SET_BIT(dispBuf[2], 16);	//G
//            else                LCD_RST_BIT(dispBuf[2], 16);	
//            break;	
//    
//        case 0x03:
//            //------------ 第6个"8"字符 ----------
//            if ((font & 0x01))  LCD_SET_BIT(dispBuf[1], 17);	//A
//            else                LCD_RST_BIT(dispBuf[1], 17);
//            if ((font & 0x02))  LCD_SET_BIT(dispBuf[2], 17);	//B
//            else                LCD_RST_BIT(dispBuf[2], 17);
//            if ((font & 0x04))  LCD_SET_BIT(dispBuf[3], 17);	//C
//            else                LCD_RST_BIT(dispBuf[3], 17);
//            if ((font & 0x08))  LCD_SET_BIT(dispBuf[4], 18);	//D
//            else                LCD_RST_BIT(dispBuf[4], 18);
//            if ((font & 0x10))  LCD_SET_BIT(dispBuf[3], 18);	//E
//            else                LCD_RST_BIT(dispBuf[3], 18);
//            if ((font & 0x20))  LCD_SET_BIT(dispBuf[1], 18);	//F
//            else                LCD_RST_BIT(dispBuf[1], 18);
//            if ((font & 0x40))  LCD_SET_BIT(dispBuf[2], 18);	//G
//            else                LCD_RST_BIT(dispBuf[2], 18);
//            break;	
//        
//        case 0x02:
//            //------------ 第7个"8"字符 ----------
//            if ((font & 0x01))  LCD_SET_BIT(dispBuf[1], 19);	//A
//            else                LCD_RST_BIT(dispBuf[1], 19);
//            if ((font & 0x02))  LCD_SET_BIT(dispBuf[2], 19);	//B
//            else                LCD_RST_BIT(dispBuf[2], 19);
//            if ((font & 0x04))  LCD_SET_BIT(dispBuf[3], 19);	//C
//            else                LCD_RST_BIT(dispBuf[3], 19);
//            if ((font & 0x08))  LCD_SET_BIT(dispBuf[4], 20);	//D
//            else                LCD_RST_BIT(dispBuf[4], 20);
//            if ((font & 0x10))  LCD_SET_BIT(dispBuf[3], 20);	//E
//            else                LCD_RST_BIT(dispBuf[3], 20);
//            if ((font & 0x20))  LCD_SET_BIT(dispBuf[1], 20);	//F
//            else                LCD_RST_BIT(dispBuf[1], 20);
//            if ((font & 0x40))  LCD_SET_BIT(dispBuf[2], 20);	//G
//            else                LCD_RST_BIT(dispBuf[2], 20);	
//            break;	
//    
//        case 0x01:
//            //------------ 第8个"8"字符 ----------
//            if ((font & 0x01))  LCD_SET_BIT(dispBuf[1], 21);	//A
//            else                LCD_RST_BIT(dispBuf[1], 21);
//            if ((font & 0x02))  LCD_SET_BIT(dispBuf[2], 21);	//B
//            else                LCD_RST_BIT(dispBuf[2], 21);
//            if ((font & 0x04))  LCD_SET_BIT(dispBuf[3], 21);	//C
//            else                LCD_RST_BIT(dispBuf[3], 21);
//            if ((font & 0x08))  LCD_SET_BIT(dispBuf[4], 22);	//D
//            else                LCD_RST_BIT(dispBuf[4], 22);
//            if ((font & 0x10))  LCD_SET_BIT(dispBuf[3], 22);	//E
//            else                LCD_RST_BIT(dispBuf[3], 22);
//            if ((font & 0x20))  LCD_SET_BIT(dispBuf[1], 22);	//F
//            else                LCD_RST_BIT(dispBuf[1], 22);
//            if ((font & 0x40))  LCD_SET_BIT(dispBuf[2], 22);	//G
//            else                LCD_RST_BIT(dispBuf[2], 22);	
//            break;
//        }
//    }
//    LcdDisplayRefresh();
//}
