#include "bsp.h"

DataBuf Nb_Buf[NB_MAX_LEN];
DataBuf Rs232_Buf[RS232_MAX_LEN];
DataBuf Rs485_Buf[RS485_MAX_LEN];

DataBuf NbTemp;
DataBuf Rs232Temp;
DataBuf Rs485Temp;

uartRec NbReceive = {0,0};
uartRec Rs232Receive = {0,0};
uartRec Rs485Receive = {0,0};

//extern MODH_T g_tModH;
/**************************************************************************************************
 * 缓冲区当前所有数据长度
 *************************************************************************************************/
unsigned char buffSize(UART_Type* UARTx)
{
	if(UARTx == UART4)
	{
    short  wi = NbReceive.writeIndex;
    short  ri = NbReceive.readIndex;
    return (wi >= ri) ? (wi - ri) : (NB_MAX_LEN - ri + wi);
	}
//	else if(USARTx == UART3)
//	{
//    short  wi = Rs232Receive.writeIndex;
//    short  ri = Rs232Receive.readIndex;
////	SendData1(wi);
////	SendData1(ri);
//    return (wi >= ri) ? (wi - ri) : (RS232_MAX_LEN - ri + wi);
//	}
//	else if(USARTx == UART5)
//	{
//    short  wi = Rs485Receive.writeIndex;
//    short  ri = Rs485Receive.readIndex;
//    return (wi >= ri) ? (wi - ri) : (RS485_MAX_LEN - ri + wi);
//	}
	return 0;
}

/**************************************************************************************************
 * 缓冲区是否空了 
返回值 :1 空  0: 非空
 *************************************************************************************************/
unsigned char isEmpty(UART_Type* UARTx)
{	
	if(UARTx == UART4)
	{
    short  wi = NbReceive.writeIndex;
    short  ri = NbReceive.readIndex;
    return (wi == ri);
	}
//	else if(USARTx == UART3)
//	{
//    short  wi = Rs232Receive.writeIndex;
//    short  ri = Rs232Receive.readIndex;
//    return (wi == ri);
//	}
//	else if(USARTx == UART5)
//	{
//    short  wi = Rs485Receive.writeIndex;
//    short  ri = Rs485Receive.readIndex;
//    return (wi == ri);
//	}
	return 1;
}

/**************************************************************************************************
 * 缓冲区是否满了
返回值  1: 满 0: 非满
 *************************************************************************************************/
unsigned char isFull(UART_Type* UARTx)
{

	if(UARTx == UART4)
	{
    short  wi = NbReceive.writeIndex;
    short  ri = NbReceive.readIndex;
    return ((wi + 1) % NB_MAX_LEN == ri);
	}
//	else if(USARTx == UART3)
//	{
//    short  wi = Rs232Receive.writeIndex;
//    short  ri = Rs232Receive.readIndex;
//    return ((wi + 1) % RS232_MAX_LEN == ri);
//	}
//	else if(USARTx == UART5)
//	{
//    short  wi = Rs485Receive.writeIndex;
//    short  ri = Rs485Receive.readIndex;
//    return ((wi + 1) % RS485_MAX_LEN == ri);
//	}
	return 1;
}

/**************************************************************************************************
 * 将串口接收到的 数据 从顶部压入堆栈
 *************************************************************************************************/

void pushBuff (UART_Type* UARTx,u8 data)
{
//	GPSBuf[GSMReceive.writeIndex].rbuf[UART1_Rx_Sta]  = data;
//  GPSBuf[GSMReceive.writeIndex].len   = UART1_Rx_Sta;
//		
//    //memcpy(uart1Buf[_uartReceive.writeIndex].rbuf,data.rbuf,data.len);          //rbuf 数值传递
//    //GPSBuf.len = data.len;                           //len数值传递
//    //writeIndex = (writeIndex + 1) % UART1_MAX_SEND_LEN;

	if(UARTx == UART4)
	{
		Nb_Buf[NbReceive.writeIndex].rbuf[Nb_Rx_Sta]  = data;
		Nb_Buf[NbReceive.writeIndex].len   = Nb_Rx_Sta + 1;
	}
//	else if(USARTx == UART3)
//	{
//		Rs232_Buf[Rs232Receive.writeIndex].rbuf[Rs232_Rx_Sta]  = data;
//		Rs232_Buf[Rs232Receive.writeIndex].len   = Rs232_Rx_Sta+1;
//	}	
////	else if(USARTx == UART5)
////	{
////		Rs485_Buf[Rs485Receive.writeIndex].rbuf[g_tModH.RxCount]  = data;
////		Rs485_Buf[Rs485Receive.writeIndex].len   = g_tModH.RxCount+1;
////	}	
	
}  

/**************************************************************************************************
 * 将堆栈里的数据从底部弹出
 *************************************************************************************************/

u8 popBuff(UART_Type* UARTx)
{
	if(UARTx == UART4)
	{
	  if(isEmpty(UART4) == 1)
	  {
			return 0;
	  }
	  memset(NbTemp.rbuf,0,60);
		memcpy(NbTemp.rbuf,Nb_Buf[NbReceive.readIndex].rbuf,Nb_Buf[NbReceive.readIndex].len);
		NbTemp.len = Nb_Buf[NbReceive.readIndex].len;
		NbReceive.readIndex = (NbReceive.readIndex + 1) % NB_MAX_LEN;
		return 1;
	}
//	else if(USARTx == UART3)
//	{
//		if(isEmpty(UART3) == 1)
//	  {
//			 return 0;
//	  }
//		memcpy(Rs232Temp.rbuf,Rs232_Buf[Rs232Receive.readIndex].rbuf,Rs232_Buf[Rs232Receive.readIndex].len);
//		Rs232Temp.len = Rs232_Buf[Rs232Receive.readIndex].len;
//		Rs232Receive.readIndex = (Rs232Receive.readIndex + 1) % RS232_MAX_LEN;
//    return 3;    
//	}
////	else if(USARTx == UART5)
////	{
////		if(isEmpty(UART5) == 1)
////	  {
////			 return 0;
////	  }
////		memcpy(g_tModH.RxBuf,Rs485_Buf[Rs485Receive.readIndex].rbuf,Rs485_Buf[Rs485Receive.readIndex].len);
////		g_tModH.len = Rs485_Buf[Rs485Receive.readIndex].len;
////		Rs485Receive.readIndex = (Rs485Receive.readIndex + 1) % RS485_MAX_LEN;
////    return 5;    
////	}
	
	return 0;
}

void clearBuff (UART_Type* UARTx)
{
	if(UARTx == UART4)
	{
	  NbReceive.readIndex = NbReceive.writeIndex;
	}
//	else if(USARTx == UART3)
//	{
//		Rs232Receive.readIndex = Rs232Receive.writeIndex;
//	}
//	else if(USARTx == UART5)
//	{
//		Rs485Receive.readIndex = Rs485Receive.writeIndex;
//	}
	
}
