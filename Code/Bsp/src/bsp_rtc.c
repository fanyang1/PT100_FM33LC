#include "bsp.h"

u8 is1min = 0;  
u8 is2hz = 0;
u8 is8hz = 0;
u8 is1hz = 0;

// RTC读写时间和秒中断示例
void RTC_IRQHandler(void)
{
	if(ENABLE == LL_RTC_IsEnabledIT_Second(RTC) &&
		 SET == LL_RTC_IsActiveFlag_Second(RTC))//查询秒中断标志是否置起
	{
    	LL_GPIO_ToggleOutputPin(GPIOC, LL_GPIO_PIN_9);
		LL_RTC_ClearFlag_Second(RTC);		//清除秒中断标志
        is1hz = 1;
	}

    if(ENABLE == LL_RTC_IsEnabledIT_Minute(RTC) &&
		 SET == LL_RTC_IsActiveFlag_Minute(RTC))//查询秒中断标志是否置起
	{
		LL_RTC_ClearFlag_Minute(RTC);		//清除秒中断标志
        is1min = 1;
	}

    if(ENABLE == LL_RTC_IsEnabledIT_2hz(RTC) &&
		 SET == LL_RTC_IsActiveFlag_2hz(RTC))//查询秒中断标志是否置起
	{
		LL_RTC_ClearFlag_2hz(RTC);		//清除秒中断标志
        is2hz = 1;
	}
    if(ENABLE == LL_RTC_IsEnabledIT_8hz(RTC) &&
		 SET == LL_RTC_IsActiveFlag_8hz(RTC))//查询秒中断标志是否置起
	{
		LL_RTC_ClearFlag_8hz(RTC);		//清除秒中断标志
        is8hz = 1;
	}
}

void RTC_Init(void)
{	

	LL_RTC_InitTypeDef   InitStructer;
    
	InitStructer.Year   = 0x20;
	InitStructer.Month  = 0x02;
	InitStructer.Day    = 0x21;	
	InitStructer.Week   = 0x04;	
	InitStructer.Hour   = 0x15;
	InitStructer.Minute = 0x33;
	InitStructer.Second = 0x00;	
	LL_RTC_Init(RTC,&InitStructer);

	LL_RTC_ClearFlag_Second(RTC);		//清除秒中断标志
	LL_RTC_EnableIT_Second(RTC);

	LL_RTC_ClearFlag_Minute(RTC);		//清除秒中断标志
	LL_RTC_EnableIT_Minute(RTC);

	LL_RTC_ClearFlag_2hz(RTC);		//清除秒中断标志
	LL_RTC_EnableIT_2hz(RTC);

	LL_RTC_ClearFlag_8hz(RTC);		//清除秒中断标志
	LL_RTC_EnableIT_8hz(RTC);

	NVIC_DisableIRQ(RTC_IRQn);				//NVIC中断控制器配置
	NVIC_SetPriority(RTC_IRQn,2);
	NVIC_EnableIRQ(RTC_IRQn);	
}



