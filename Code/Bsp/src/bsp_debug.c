#include "bsp.h"

#define Debug_Print  1   //Debug_Printf 打印开关    0:禁止打印  1：使能打印
#define Debug_UART   UART0

void Debug_Printf(char *format, ...)
{
#if Debug_Print
	
    va_list   v_args;

    va_start(v_args, format);
    vprintf(format,v_args);  
    va_end(v_args);	
#endif
}


void bsp_DebugInit(void)
{
	bsp_Uartx_Init(Debug_UART, 115200);
}

void Debug_SendString(uint08* buf)
{
	while(*buf)                  //检测字符串结束符
	{
        LL_UART_TransmitData(Debug_UART, *buf++);		//将发送数据写入发送寄存器
		while(SET != LL_UART_IsActiveFlag_TXSE(Debug_UART));		//等待发送完成
	}
}

void Debug_SendData(u8* buf,u8 len)
{
  uint08 i;
	
	for(i=0;i<len;i++)
	{
        LL_UART_TransmitData(Debug_UART, buf[i]);		//将发送数据写入发送寄存器
		while(SET != LL_UART_IsActiveFlag_TXSE(Debug_UART));		//等待发送完成
	}
}

void Debug_SendOneData(u8 buf)
{
    LL_UART_TransmitData(Debug_UART, buf);		//将发送数据写入发送寄存器
    while(SET != LL_UART_IsActiveFlag_TXSE(Debug_UART));		//等待发送完成
}

//重定义fputc函数 
int fputc(int ch, FILE *f)
{ 
	LL_UART_TransmitData(Debug_UART, (uint08)ch);		//将发送数据写入发送寄存器
	while(SET != LL_UART_IsActiveFlag_TXSE(Debug_UART));		//等待发送完成
	return ch;
}

