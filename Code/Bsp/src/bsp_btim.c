#include "bsp.h"

extern u8 Uart4_OverTime;
extern u8 Nb_Rx_Sta;
void bsp_btim_init(void)
{
    LL_BSTIM_InitTypeDef   InitStructer;
    
    InitStructer.Prescaler         = 9;
    InitStructer.Autoreload        = 799;    //900us��ʱ��ʱ��
    InitStructer.AutoreloadState   = ENABLE;
    InitStructer.ClockSource       = LL_RCC_BSTIM_OPERATION_CLK_SOURCE_APBCLK2;
  
    LL_BSTIM_Init(BSTIM,&InitStructer);
  
    NVIC_DisableIRQ(BSTIM_IRQn);
    NVIC_SetPriority(BSTIM_IRQn, 2);
    NVIC_EnableIRQ(BSTIM_IRQn);
    
    LL_BSTIM_ClearFlag_UpdataEvent(BSTIM);
    LL_BSTIM_EnabledIT_UpdataEvent(BSTIM);
  
    LL_BSTIM_EnableCounter(BSTIM);
}

void BSTIM_IRQHandler(void)
{
    if (( LL_BSTIM_IsEnabledIT_UpdataEvent(BSTIM) == SET ) &&(LL_BSTIM_IsActiveFlag_UpdataEvent(BSTIM)==SET))
    {	 
        LL_BSTIM_ClearFlag_UpdataEvent(BSTIM);    
    }			
}
