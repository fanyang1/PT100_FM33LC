/*
*********************************************************************************************************
*
*	模块名称 : BSP模块(For FM33LC0)
*	文件名称 : bsp.c
*	版    本 : V1.0
*	说    明 : 这是硬件底层驱动程序模块的主文件。主要提供 bsp_Init()函数供主程序调用。主程序的每个c文件可以在开
*			  头	添加 #include "bsp.h" 来包含所有的外设驱动模块。
*
*	修改记录 :
*		版本号  日期        作者     说明
*		V1.0    2020-04-14   FAN   正式发布
*
*
*********************************************************************************************************
*/
#include "bsp.h"

#define EXAMPLE_NAME	"PT100"
#define EXAMPLE_VER	    "V1.0"
#define EXAMPLE_AUTHOR	"DING JIE"

static void PrintfLogo(void);

/*
*********************************************************************************************************
*	函 数 名: bsp_Init
*	功能说明: 初始化硬件设备。只需要调用一次。该函数配置CPU寄存器和外设的寄存器并初始化一些全局变量。
*			 全局变量。
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_Init(void)
{
  //RCHF温度调校函数，RCHF=8MHZ全温区+/-2%，RCHF8MHZ以上需要进行温度调校。
  //调校的前提是有比较精准的外部32K晶体
  //调校按实际需求可以采用定时（时间用户自己确定）调校;也可以采用ADC查询温度，温差大于一定值再进行调校
  //任何一路ETIM都可以 
//  RCHF_Adj_Proc(ETIM1, clkmode);



  bsp_GPIO();
//  
//  bsp_LPUART_Init(9600);
//	
//  bsp_LCDinit();
  
//  bsp_lptimer_init();     //初始化LPTIMER
//  
//  bsp_lptimer_start();  //启动LPTIMER
	
//  open_beep();  
//  
  RTC_Init();

  bsp_adcInit();
	
//	bsp_Uartx_Init(UART5,2400);   //红外串口初始化
	
//  bsp_init_svd();
  
//  bsp_btim_init();

  bsp_DebugInit();
  
//  bsp_Uartx_Init(UART4, 57600);//NB串口初始化
  
  PrintfLogo();

//  w25qxx_init();
  I2C_Init();

  LcdInit();

  LCD_ShowALL();

  DelayMs(1000);
}


static void PrintfLogo(void)
{
  Debug_Printf("*************************************************************\r\n");
	
  Debug_Printf("* Name    :     %s\r\n", EXAMPLE_NAME);	
  Debug_Printf("* Version :     %s\r\n", EXAMPLE_VER);		
  Debug_Printf("* Author  :     %s\r\n", EXAMPLE_AUTHOR);

  Debug_Printf("*************************************************************\r\n\r\n");
}
	
