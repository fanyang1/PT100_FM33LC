#include "bsp.h"  

uint08 IsWkup = 0;

// Sleep
void Sleep(void)
{
    LL_PMU_SleepInitTypeDef LPM_InitStruct;
    
    LL_RMU_EnablePowerDownReset(RMU);//打开PDR
    LL_RMU_DisableBORPowerDownReset(RMU);//关闭BOR 2uA
    
    LL_ADC_Disable(ADC);//关闭ADC
    LL_VREF_DisableVREF(VREF);//关闭VREF1p2
    WRITE_REG(VREF->BUFCR, 0);//关闭全部VREFbuf
    
    LPM_InitStruct.DeepSleep = LL_PMU_SLEEP_MODE_NOMAL;
    LPM_InitStruct.PowerMode = LL_PMU_POWER_MODE_SLEEP_AND_DEEPSLEEP;
    LPM_InitStruct.WakeupFrequency = LL_PMU_SLEEP_WAKEUP_FREQ_RCHF_8MHZ;
    LPM_InitStruct.WakeupDelay = LL_PMU_WAKEUP_DELAY_TIME_2US;
    LPM_InitStruct.CoreVoltageScaling = DISABLE; 
    LL_PMU_Sleep_Init(PMU, &LPM_InitStruct);
}

//DeepSleep
void DeepSleep(void)
{
    LL_PMU_SleepInitTypeDef LPM_InitStruct;

    LL_RMU_EnablePowerDownReset(RMU);//打开PDR
    LL_RMU_DisableBORPowerDownReset(RMU);//关闭BOR 2uA
    
    LL_ADC_Disable(ADC);//关闭ADC
    LL_VREF_DisableVREF(VREF);//关闭VREF1p2
    WRITE_REG(VREF->BUFCR, 0);//关闭全部VREFbuf
    
    LPM_InitStruct.DeepSleep = LL_PMU_SLEEP_MODE_DEEP;
    LPM_InitStruct.PowerMode = LL_PMU_POWER_MODE_SLEEP_AND_DEEPSLEEP;
    LPM_InitStruct.WakeupFrequency = LL_PMU_SLEEP_WAKEUP_FREQ_RCHF_8MHZ;
    LPM_InitStruct.WakeupDelay = LL_PMU_WAKEUP_DELAY_TIME_2US;
    LPM_InitStruct.CoreVoltageScaling = DISABLE; 
    LL_PMU_Sleep_Init(PMU, &LPM_InitStruct);
}

//DeepSleep 关XTLF与RClP
void DeepSleep32768OFF(void)
{
    LL_PMU_SleepInitTypeDef LPM_InitStruct;

    LL_RCC_Disable_XTLF();//关闭XTLF
    LL_RCC_SetSleepModeRCLPWorkMode(LL_RCC_RCLP_UNDER_SLEEP_CLOSE);//休眠下关闭RCLP
    
    LL_RMU_EnablePowerDownReset(RMU);//打开PDR
    LL_RMU_DisableBORPowerDownReset(RMU);//关闭BOR 2uA
    
    LL_ADC_Disable(ADC);//关闭ADC
    LL_VREF_DisableVREF(VREF);//关闭VREF1p2
    WRITE_REG(VREF->BUFCR, 0);//关闭全部VREFbuf
    
    LPM_InitStruct.DeepSleep = LL_PMU_SLEEP_MODE_DEEP;
    LPM_InitStruct.PowerMode = LL_PMU_POWER_MODE_SLEEP_AND_DEEPSLEEP;
    LPM_InitStruct.WakeupFrequency = LL_PMU_SLEEP_WAKEUP_FREQ_RCHF_8MHZ;
    LPM_InitStruct.WakeupDelay = LL_PMU_WAKEUP_DELAY_TIME_2US;
    LPM_InitStruct.CoreVoltageScaling = ENABLE; 
    LL_PMU_Sleep_Init(PMU, &LPM_InitStruct);
}










//void NMI_Handler(void)
//{
//    //NWKUP默认连接到了cpu的NMI不可屏蔽中断，不受NVIC控制，不受全局中断使能控制，唤醒后必然进NMI中断
//    if( SET == LL_PMU_IsActiveFlag_WakeupPIN(PMU , LL_PMU_WKUP0PIN) )
//    {    
//        LL_PMU_ClearFlag_WakeupPIN(PMU , LL_PMU_WKUP0PIN);
//    }
////    if( SET == LL_PMU_IsActiveFlag_WakeupPIN(PMU , LL_PMU_WKUP1PIN) )
////    {    
////        LL_PMU_ClearFlag_WakeupPIN(PMU , LL_PMU_WKUP1PIN);
////    }
////    if( SET == LL_PMU_IsActiveFlag_WakeupPIN(PMU , LL_PMU_WKUP2PIN) )
////    {    
////        LL_PMU_ClearFlag_WakeupPIN(PMU , LL_PMU_WKUP2PIN);
////    }
////    if( SET == LL_PMU_IsActiveFlag_WakeupPIN(PMU , LL_PMU_WKUP3PIN) )
////    {    
////        LL_PMU_ClearFlag_WakeupPIN(PMU , LL_PMU_WKUP3PIN);
////    }
////    if( SET == LL_PMU_IsActiveFlag_WakeupPIN(PMU , LL_PMU_WKUP4PIN) )
////    {    
////        LL_PMU_ClearFlag_WakeupPIN(PMU , LL_PMU_WKUP4PIN);
////    }
////    if( SET == LL_PMU_IsActiveFlag_WakeupPIN(PMU , LL_PMU_WKUP5PIN) )
////    {    
////        LL_PMU_ClearFlag_WakeupPIN(PMU , LL_PMU_WKUP5PIN);
////    }
////    if( SET == LL_PMU_IsActiveFlag_WakeupPIN(PMU , LL_PMU_WKUP6PIN) )
////    {    
////        LL_PMU_ClearFlag_WakeupPIN(PMU , LL_PMU_WKUP6PIN);
////    }
//    if( SET == LL_PMU_IsActiveFlag_WakeupPIN(PMU , LL_PMU_WKUP7PIN) )
//    {    
////				LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_0); //WAKEUP nb
////				DelayUs(300);
////				LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_0); //WAKEUP nb
////				IsWkup = 1;
//				Module.TimeWake = 180;
//				Maintask_Status = State_Meter;
//		  	nbwkupState = 0 ;
//			
//        LL_PMU_ClearFlag_WakeupPIN(PMU , LL_PMU_WKUP7PIN);
//           
//    }    
//}

//// WKUP引脚中断初始化
//void WKUP_init(void)
//{
//LL_GPIO_InitTypeDef GPIO_InitStruct = {0};    

//    //WKUP0
//    //PA15设为输入 
//    GPIO_InitStruct.Pin = LL_GPIO_PIN_6;
//    GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
//    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
//    GPIO_InitStruct.Pull = DISABLE;
//    LL_GPIO_Init(GPIOD, &GPIO_InitStruct);
//    //WKUP 使能
//    LL_GPIO_SetWkupEntry(GPIO_COMMON, LL_GPIO_WKUP_INT_ENTRY_NMI);//NMI中断入口
//    LL_GPIO_SetWkupPolarity(GPIO_COMMON, LL_GPIO_WKUP_7, LL_GPIO_WKUP_POLARITY_FALLING);//下降沿唤醒
//    LL_GPIO_EnableWkup(GPIO_COMMON, LL_GPIO_WKUP_7);//使能NWKUP功能
//    
////    //WKUP1
////    //PA10设为输入 
////    GPIO_InitStruct.Pin = LL_GPIO_PIN_10;
////    GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
////    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
////    GPIO_InitStruct.Pull = DISABLE;
////    LL_GPIO_Init(GPIOA, &GPIO_InitStruct);
////    //WKUP 使能
////    LL_GPIO_SetWkupEntry(GPIO_COMMON, LL_GPIO_WKUP_INT_ENTRY_NMI);//NMI中断入口
////    LL_GPIO_SetWkupPolarity(GPIO_COMMON, LL_GPIO_WKUP_1, LL_GPIO_WKUP_POLARITY_FALLING);//下降沿唤醒
////    LL_GPIO_EnableWkup(GPIO_COMMON, LL_GPIO_WKUP_1);//使能NWKUP功能    
//    
////    //WKUP2
////    //PB2设为输入 
////    GPIO_InitStruct.Pin = LL_GPIO_PIN_2;
////    GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
////    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
////    GPIO_InitStruct.Pull = DISABLE;
////    LL_GPIO_Init(GPIOB, &GPIO_InitStruct);
////    //WKUP 使能
////    LL_GPIO_SetWkupEntry(GPIO_COMMON, LL_GPIO_WKUP_INT_ENTRY_NMI);//NMI中断入口
////    LL_GPIO_SetWkupPolarity(GPIO_COMMON, LL_GPIO_WKUP_2, LL_GPIO_WKUP_POLARITY_FALLING);//下降沿唤醒
////    LL_GPIO_EnableWkup(GPIO_COMMON, LL_GPIO_WKUP_2);//使能NWKUP功能    

////    //WKUP3
////    //PB12设为输入 
////    GPIO_InitStruct.Pin = LL_GPIO_PIN_12;
////    GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
////    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
////    GPIO_InitStruct.Pull = DISABLE;
////    LL_GPIO_Init(GPIOB, &GPIO_InitStruct);
////    //WKUP 使能
////    LL_GPIO_SetWkupEntry(GPIO_COMMON, LL_GPIO_WKUP_INT_ENTRY_NMI);//NMI中断入口
////    LL_GPIO_SetWkupPolarity(GPIO_COMMON, LL_GPIO_WKUP_3, LL_GPIO_WKUP_POLARITY_FALLING);//下降沿唤醒
////    LL_GPIO_EnableWkup(GPIO_COMMON, LL_GPIO_WKUP_3);//使能NWKUP功能    
//    
////    //WKUP4
////    //PC6设为输入 
////    GPIO_InitStruct.Pin = LL_GPIO_PIN_6;
////    GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
////    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
////    GPIO_InitStruct.Pull = DISABLE;
////    LL_GPIO_Init(GPIOC, &GPIO_InitStruct);
////    //WKUP 使能
////    LL_GPIO_SetWkupEntry(GPIO_COMMON, LL_GPIO_WKUP_INT_ENTRY_NMI);//NMI中断入口
////    LL_GPIO_SetWkupPolarity(GPIO_COMMON, LL_GPIO_WKUP_4, LL_GPIO_WKUP_POLARITY_FALLING);//下降沿唤醒
////    LL_GPIO_EnableWkup(GPIO_COMMON, LL_GPIO_WKUP_4);//使能NWKUP功能        
//    
////    //WKUP5
////    //PC10设为输入 
////    GPIO_InitStruct.Pin = LL_GPIO_PIN_10;
////    GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
////    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
////    GPIO_InitStruct.Pull = DISABLE;
////    LL_GPIO_Init(GPIOC, &GPIO_InitStruct);
////    //WKUP 使能
////    LL_GPIO_SetWkupEntry(GPIO_COMMON, LL_GPIO_WKUP_INT_ENTRY_NMI);//NMI中断入口
////    LL_GPIO_SetWkupPolarity(GPIO_COMMON, LL_GPIO_WKUP_5, LL_GPIO_WKUP_POLARITY_FALLING);//下降沿唤醒
////    LL_GPIO_EnableWkup(GPIO_COMMON, LL_GPIO_WKUP_5);//使能NWKUP功能    

////    //WKUP6
////    //PD11设为输入 
////    GPIO_InitStruct.Pin = LL_GPIO_PIN_11;
////    GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
////    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
////    GPIO_InitStruct.Pull = DISABLE;
////    LL_GPIO_Init(GPIOD, &GPIO_InitStruct);
////    //WKUP 使能
////    LL_GPIO_SetWkupEntry(GPIO_COMMON, LL_GPIO_WKUP_INT_ENTRY_NMI);//NMI中断入口
////    LL_GPIO_SetWkupPolarity(GPIO_COMMON, LL_GPIO_WKUP_6, LL_GPIO_WKUP_POLARITY_FALLING);//下降沿唤醒
////    LL_GPIO_EnableWkup(GPIO_COMMON, LL_GPIO_WKUP_6);//使能NWKUP功能    
//    
////    //WKUP7
////    //PD6设为输入 
////    GPIO_InitStruct.Pin = LL_GPIO_PIN_6;
////    GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
////    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
////    GPIO_InitStruct.Pull = DISABLE;
////    LL_GPIO_Init(GPIOD, &GPIO_InitStruct);
////    //WKUP 使能
////    LL_GPIO_SetWkupEntry(GPIO_COMMON, LL_GPIO_WKUP_INT_ENTRY_NMI);//NMI中断入口
////    LL_GPIO_SetWkupPolarity(GPIO_COMMON, LL_GPIO_WKUP_7, LL_GPIO_WKUP_POLARITY_FALLING);//下降沿唤醒
////    LL_GPIO_EnableWkup(GPIO_COMMON, LL_GPIO_WKUP_7);//使能NWKUP功能    

//}


///*
////以下是wkup使用38号中断入口进行中断 只列举了wkup0 ，wkup1~7请参照上面NMI中断自己加
//void WKUP_IRQHandler(void)
//{
//    if( SET == LL_PMU_IsActiveFlag_WakeupPIN(PMU , LL_PMU_WKUP0PIN) )
//    {    
//        LL_PMU_ClearFlag_WakeupPIN(PMU , LL_PMU_WKUP0PIN);
//    }
//}

//// 外部引脚中断初始化
//void WKUP_init(void)
//{
//LL_GPIO_InitTypeDef GPIO_InitStruct = {0};    

//    //WKUP0
//    //PA15设为输入 
//    GPIO_InitStruct.Pin = LL_GPIO_PIN_15;
//    GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
//    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
//    GPIO_InitStruct.Pull = DISABLE;
//    LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

//    //WKUP 使能
//    LL_GPIO_SetWkupEntry(GPIO_COMMON, LL_GPIO_WKUP_INT_ENTRY_NUM_38);//NMI中断入口
//    LL_GPIO_SetWkupPolarity(GPIO_COMMON, LL_GPIO_WKUP_0, LL_GPIO_WKUP_POLARITY_FALLING);//下降沿唤醒

//	//NVIC中断配置
//	NVIC_DisableIRQ(WKUP_IRQn);
//	NVIC_SetPriority(WKUP_IRQn,2);//中断优先级配置
//	NVIC_EnableIRQ(WKUP_IRQn);	

//    //使能NWKUP功能
//    LL_GPIO_EnableWkup(GPIO_COMMON, LL_GPIO_WKUP_0);
//}
//*/
