#ifndef __BSP_UART_H__
#define __BSP_UART_H__

#include "bsp.h"  

extern u8 Nb_Rx_Sta;
extern u8 Rs232_Rx_Sta;
extern u8 Rs485_Rx_Sta;

struct UARTOpStruct
{
	u8 TxBuf[255];	//发送数据指针
	u8 TxLen;	//待发送数据长度
	u8 TxOpc;	//已发送数据长度	
	u8 RxBuf[255];	//接收数据指针
	u8 RxLen;	//待接收数据长度
	u8 RxOpc;	//已接收数据长度

	u8 rx_buf;      //UART中断读取字节
	u8 tm_rx_ov;    //接收字节超时
	u8 tm_tx_ov;    //发送一帧数据超时
	u8 StaUART0RxStart;  //UART0开始有效接收数据
	u8 StaUART0RxEnd;    //UART0接收一帧数据完成
	u8 StaUART0Sending;  //UART0开始有效发送数据
};

extern struct UARTOpStruct UARTxOp[6];

extern u8 UART_Status;

//void Test_Uartx(UARTx_Type* UARTx,u32 baud);
//void bsp_Uartx_Init(UARTx_Type* UARTx, u32 baud);
//void UART4_SendString(uint08* buf);
//void UART4_SendData(u8* buf,u8 len);
void bsp_Uartx_Init(UART_Type* UARTx,unsigned int baud);
void Test_Uartx(UART_Type* UARTx);


#endif
