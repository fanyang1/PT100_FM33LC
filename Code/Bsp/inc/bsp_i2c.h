#ifndef __BSP_I2C_H
#define __BSP_I2C_H

#include "bsp.h"

#define	STARTBIT	0	
#define	RESTARTBIT	1
#define	STOPBIT		2

uint8_t I2C_Send_Bit(uint8_t BIT_def ) ;
uint8_t I2C_Send_Byte( uint8_t x_byte );
uint8_t I2C_Receive_Byte( uint8_t *x_byte ) ;
void I2C_Init(void);

uint8_t AD_INIT(void);
uint8_t get_ad(void);


#endif
