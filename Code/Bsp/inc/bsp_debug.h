#ifndef __BSP_DEBUG_H
#define __BSP_DEBUG_H

#include "bsp.h"

void Debug_Printf(char *format, ...);
void bsp_DebugInit(void);

void Debug_SendString(uint08* buf);
void Debug_SendData(uint08* buf,uint08 len);
void Debug_SendOneData(u8 buf);

#endif


