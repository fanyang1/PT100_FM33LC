#ifndef __LCDZHEN_H__
#define __LCDZHEN_H__

#define LCD_SET_BIT(data, bit)  ((data) |= (0x1 << (bit)))
#define LCD_RST_BIT(data, bit)  ((data) &= ~(0x1 << (bit)))


    #define SET_VALUE_1D                      LCD_SET_BIT(DISPDATA[3], 14)    //(DISPDATA[3]|= (1<<6))
    #define RESET_VALUE_1D                    LCD_RST_BIT(DISPDATA[3], 14)    //(DISPDATA[3]&= ~(1<<6))

//#define SET_VALUE_DP1                     (DISPDATA[3] |= 0x00000020)    //(DISPDATA[3]|= (1<<5))
//#define RESET_VALUE_DP1                   (DISPDATA[3] &= 0xffffffdf)    //(DISPDATA[3]&= ~(1<<5))
//#define SET_VALUE_2D                      (DISPDATA[3] |= 0x00000010)    //(DISPDATA[3]|= (1<<4))
//#define RESET_VALUE_2D                    (DISPDATA[3] &= 0xffffffef)    //(DISPDATA[3]&= ~(1<<4))
//#define SET_VALUE_DP2                     (DISPDATA[3] |= 0x00000008)    //(DISPDATA[3]|= (1<<3))
//#define RESET_VALUE_DP2                   (DISPDATA[3] &= 0xfffffff7)    //(DISPDATA[3]&= ~(1<<3))
//#define SET_VALUE_3D                      (DISPDATA[3] |= 0x00000004)    //(DISPDATA[3]|= (1<<2))
//#define RESET_VALUE_3D                    (DISPDATA[3] &= 0xfffffffb)    //(DISPDATA[3]&= ~(1<<2))
//#define SET_VALUE_DP3                     (DISPDATA[3] |= 0x00000002)    //(DISPDATA[3]|= (1<<1))
//#define RESET_VALUE_DP3                   (DISPDATA[3] &= 0xfffffffd)    //(DISPDATA[3]&= ~(1<<1))
//#define SET_VALUE_4D                      (DISPDATA[3] |= 0x00000001)    //(DISPDATA[3]|= (1<<0))
//#define RESET_VALUE_4D                    (DISPDATA[3] &= 0xfffffffe)    //(DISPDATA[3]&= ~(1<<0))
//#define SET_VALUE_COL1                    (DISPDATA[5] |= 0x00002000)    //(DISPDATA[5]|= (1<<13))
//#define RESET_VALUE_COL1                  (DISPDATA[5] &= 0xffffdfff)    //(DISPDATA[5]&= ~(1<<13))
//#define SET_VALUE_COL2                    (DISPDATA[5] |= 0x00001000)    //(DISPDATA[5]|= (1<<12))
//#define RESET_VALUE_COL2                  (DISPDATA[5] &= 0xffffefff)    //(DISPDATA[5]&= ~(1<<12))
    #define SET_VALUE_W5                      LCD_SET_BIT(DISPDATA[3], 27)    //(DISPDATA[5]|= (1<<15))
    #define RESET_VALUE_W5                    LCD_RST_BIT(DISPDATA[3], 27)    //(DISPDATA[5]&= ~(1<<15))
//#define SET_VALUE_L1                      (DISPDATA[5] |= 0x00004000)    //(DISPDATA[5]|= (1<<14))
//#define RESET_VALUE_L1                    (DISPDATA[5] &= 0xffffbfff)    //(DISPDATA[5]&= ~(1<<14))
    #define SET_VALUE_5F                      LCD_SET_BIT(DISPDATA[3], 9)    //(DISPDATA[3]|= (1<<7))
    #define RESET_VALUE_5F                    LCD_RST_BIT(DISPDATA[3], 9)    //(DISPDATA[3]&= ~(1<<7))

    #define SET_VALUE_5A                      LCD_SET_BIT(DISPDATA[3], 8)    //(DISPDATA[3]|= (1<<8))
    #define RESET_VALUE_5A                    LCD_RST_BIT(DISPDATA[3], 8)    //(DISPDATA[3]&= ~(1<<8))

    #define SET_VALUE_6F                      LCD_SET_BIT(DISPDATA[3], 7)    //(DISPDATA[3]|= (1<<9))
    #define RESET_VALUE_6F                    LCD_RST_BIT(DISPDATA[3], 7)    //(DISPDATA[3]&= ~(1<<9))

    #define SET_VALUE_6A                      LCD_SET_BIT(DISPDATA[3], 6)    //(DISPDATA[3]|= (1<<10))
    #define RESET_VALUE_6A                    LCD_RST_BIT(DISPDATA[3], 6)    //(DISPDATA[3]&= ~(1<<10))

    #define SET_VALUE_7F                      LCD_SET_BIT(DISPDATA[3], 5)    //(DISPDATA[3]|= (1<<11))
    #define RESET_VALUE_7F                    LCD_RST_BIT(DISPDATA[3], 5)    //(DISPDATA[3]&= ~(1<<11))

    #define SET_VALUE_7A                      LCD_SET_BIT(DISPDATA[3], 4)    //(DISPDATA[3]|= (1<<12))
    #define RESET_VALUE_7A                    LCD_RST_BIT(DISPDATA[3], 4)    //(DISPDATA[3]&= ~(1<<12))

//#define SET_VALUE_S4                      (DISPDATA[3] |= 0x00002000)    //(DISPDATA[3]|= (1<<13))
//#define RESET_VALUE_S4                    (DISPDATA[3] &= 0xffffdfff)    //(DISPDATA[3]&= ~(1<<13))
//#define SET_VALUE_S5                      (DISPDATA[3] |= 0x00004000)    //(DISPDATA[3]|= (1<<14))
//#define RESET_VALUE_S5                    (DISPDATA[3] &= 0xffffbfff)    //(DISPDATA[3]&= ~(1<<14))
    #define SET_VALUE_8F                      LCD_SET_BIT(DISPDATA[3], 1)    //(DISPDATA[3]|= (1<<15))
    #define RESET_VALUE_8F                    LCD_RST_BIT(DISPDATA[3], 1)   //(DISPDATA[3]&= ~(1<<15))

    #define SET_VALUE_8A                      LCD_SET_BIT(DISPDATA[3], 0)    //(DISPDATA[3]|= (1<<16))
    #define RESET_VALUE_8A                    LCD_RST_BIT(DISPDATA[3], 0)    //(DISPDATA[3]&= ~(1<<16))

    #define SET_VALUE_9F                      DISPDATA[3] |= 0x80000000    //(DISPDATA[3]|= (1<<17))
    #define RESET_VALUE_9F                    DISPDATA[3] &= 0x7fffFfff    //(DISPDATA[3]&= ~(1<<17))

    #define SET_VALUE_9A                      DISPDATA[3] |= 0x40000000    //(DISPDATA[3]|= (1<<18))
    #define RESET_VALUE_9A                    DISPDATA[3] &= 0xBfffFfff    //(DISPDATA[3]&= ~(1<<18))

    #define SET_VALUE_10F                     LCD_SET_BIT(DISPDATA[3], 29)    //(DISPDATA[3]|= (1<<19))
    #define RESET_VALUE_10F                   LCD_RST_BIT(DISPDATA[3], 29)    //(DISPDATA[3]&= ~(1<<19))

    #define SET_VALUE_10A                     LCD_SET_BIT(DISPDATA[3], 28)    //(DISPDATA[3]|= (1<<26))
    #define RESET_VALUE_10A                   LCD_RST_BIT(DISPDATA[3], 28)    //(DISPDATA[3]&= ~(1<<26))

    #define SET_VALUE_1E                      LCD_SET_BIT(DISPDATA[2], 14)    //(DISPDATA[2]|= (1<<6))
    #define RESET_VALUE_1E                    LCD_RST_BIT(DISPDATA[2], 14)    //(DISPDATA[2]&= ~(1<<6))

    #define SET_VALUE_1C                      LCD_SET_BIT(DISPDATA[2], 13)    //(DISPDATA[2]|= (1<<5))
    #define RESET_VALUE_1C                    LCD_RST_BIT(DISPDATA[2], 13)    //(DISPDATA[2]&= ~(1<<5))

//#define SET_VALUE_2E                      (DISPDATA[2] |= 0x00000010)    //(DISPDATA[2]|= (1<<4))
//#define RESET_VALUE_2E                    (DISPDATA[2] &= 0xffffffef)    //(DISPDATA[2]&= ~(1<<4))
//#define SET_VALUE_2C                      (DISPDATA[2] |= 0x00000008)    //(DISPDATA[2]|= (1<<3))
//#define RESET_VALUE_2C                    (DISPDATA[2] &= 0xfffffff7)    //(DISPDATA[2]&= ~(1<<3))
//#define SET_VALUE_3E                      (DISPDATA[2] |= 0x00000004)    //(DISPDATA[2]|= (1<<2))
//#define RESET_VALUE_3E                    (DISPDATA[2] &= 0xfffffffb)    //(DISPDATA[2]&= ~(1<<2))
//#define SET_VALUE_3C                      (DISPDATA[2] |= 0x00000002)    //(DISPDATA[2]|= (1<<1))
//#define RESET_VALUE_3C                    (DISPDATA[2] &= 0xfffffffd)    //(DISPDATA[2]&= ~(1<<1))
//#define SET_VALUE_4E                      (DISPDATA[2] |= 0x00000001)    //(DISPDATA[2]|= (1<<0))
//#define RESET_VALUE_4E                    (DISPDATA[2] &= 0xfffffffe)    //(DISPDATA[2]&= ~(1<<0))
//#define SET_VALUE_4C                      (DISPDATA[5] |= 0x00000002)    //(DISPDATA[5]|= (1<<1))
//#define RESET_VALUE_4C                    (DISPDATA[5] &= 0xfffffffd)    //(DISPDATA[5]&= ~(1<<1))
//#define SET_VALUE_COL3                    (DISPDATA[5] |= 0x00000001)    //(DISPDATA[5]|= (1<<0))
//#define RESET_VALUE_COL3                  (DISPDATA[5] &= 0xfffffffe)    //(DISPDATA[5]&= ~(1<<0))
    #define SET_VALUE_W4                      LCD_SET_BIT(DISPDATA[2], 27)    //(DISPDATA[5]|= (1<<3))
    #define RESET_VALUE_W4                    LCD_RST_BIT(DISPDATA[2], 27)   //(DISPDATA[5]&= ~(1<<3))
//#define SET_VALUE_L2                      (DISPDATA[5] |= 0x00000004)    //(DISPDATA[5]|= (1<<2))
//#define RESET_VALUE_L2                    (DISPDATA[5] &= 0xfffffffb)    //(DISPDATA[5]&= ~(1<<2))

    #define SET_VALUE_5G                      LCD_SET_BIT(DISPDATA[2], 9)    //(DISPDATA[2]|= (1<<7))
    #define RESET_VALUE_5G                    LCD_RST_BIT(DISPDATA[2], 9)    //(DISPDATA[2]&= ~(1<<7))

    #define SET_VALUE_5B                      LCD_SET_BIT(DISPDATA[2], 8)    //(DISPDATA[2]|= (1<<8))
    #define RESET_VALUE_5B                    LCD_RST_BIT(DISPDATA[2], 8)    //(DISPDATA[2]&= ~(1<<8))

    #define SET_VALUE_6G                      LCD_SET_BIT(DISPDATA[2], 7)    //(DISPDATA[2]|= (1<<9))
    #define RESET_VALUE_6G                    LCD_RST_BIT(DISPDATA[2], 7)    //(DISPDATA[2]&= ~(1<<9))

    #define SET_VALUE_6B                      LCD_SET_BIT(DISPDATA[2], 6)    //(DISPDATA[2]|= (1<<10))
    #define RESET_VALUE_6B                    LCD_RST_BIT(DISPDATA[2], 6)    //(DISPDATA[2]&= ~(1<<10))

    #define SET_VALUE_7G                      LCD_SET_BIT(DISPDATA[2], 5)    //(DISPDATA[2]|= (1<<11))
    #define RESET_VALUE_7G                    LCD_RST_BIT(DISPDATA[2], 5)    //(DISPDATA[2]&= ~(1<<11))

    #define SET_VALUE_7B                      LCD_SET_BIT(DISPDATA[2], 4)    //(DISPDATA[2]|= (1<<12))
    #define RESET_VALUE_7B                    LCD_RST_BIT(DISPDATA[2], 4)    //(DISPDATA[2]&= ~(1<<12))

//#define SET_VALUE_S3                      (DISPDATA[2] |= 0x00002000)    //(DISPDATA[2]|= (1<<13))
//#define RESET_VALUE_S3                    (DISPDATA[2] &= 0xffffdfff)    //(DISPDATA[2]&= ~(1<<13))
//#define SET_VALUE_S6                      (DISPDATA[2] |= 0x00004000)    //(DISPDATA[2]|= (1<<14))
//#define RESET_VALUE_S6                    (DISPDATA[2] &= 0xffffbfff)    //(DISPDATA[2]&= ~(1<<14))
    #define SET_VALUE_8G                      LCD_SET_BIT(DISPDATA[2], 1)    //(DISPDATA[2]|= (1<<15))
    #define RESET_VALUE_8G                    LCD_RST_BIT(DISPDATA[2], 1)    //(DISPDATA[2]&= ~(1<<15))

    #define SET_VALUE_8B                      LCD_SET_BIT(DISPDATA[2], 0)    //(DISPDATA[2]|= (1<<16))
    #define RESET_VALUE_8B                    LCD_RST_BIT(DISPDATA[2], 0)    //(DISPDATA[2]&= ~(1<<16))

    #define SET_VALUE_9G                      DISPDATA[2] |= 0x80000000    //(DISPDATA[2]|= (1<<17))
    #define RESET_VALUE_9G                    DISPDATA[2] &= 0x7fffFfff    //(DISPDATA[2]&= ~(1<<17))

    #define SET_VALUE_9B                      DISPDATA[2] |= 0x40000000    //(DISPDATA[2]|= (1<<18))
    #define RESET_VALUE_9B                    DISPDATA[2] &= 0xBfffFfff    //(DISPDATA[2]&= ~(1<<18))

    #define SET_VALUE_10G                     LCD_SET_BIT(DISPDATA[2], 29)    //(DISPDATA[2]|= (1<<19))
    #define RESET_VALUE_10G                   LCD_RST_BIT(DISPDATA[2], 29)    //(DISPDATA[2]&= ~(1<<19))

    #define SET_VALUE_10B                     LCD_SET_BIT(DISPDATA[2], 28)    //(DISPDATA[2]|= (1<<26))
    #define RESET_VALUE_10B                   LCD_RST_BIT(DISPDATA[2], 28)    //(DISPDATA[2]&= ~(1<<26))

    #define SET_VALUE_1G                      LCD_SET_BIT(DISPDATA[1], 14)    //(DISPDATA[0]|= (1<<6))
    #define RESET_VALUE_1G                    LCD_RST_BIT(DISPDATA[1], 14)    //(DISPDATA[0]&= ~(1<<6))

    #define SET_VALUE_1B                      LCD_SET_BIT(DISPDATA[1], 13)    //(DISPDATA[0]|= (1<<5))
    #define RESET_VALUE_1B                    LCD_RST_BIT(DISPDATA[1], 13)    //(DISPDATA[0]&= ~(1<<5))

//#define SET_VALUE_2G                      LCD_RST_BIT(DISPDATA[1], 13)    //(DISPDATA[0]|= (1<<4))
//#define RESET_VALUE_2G                    LCD_RST_BIT(DISPDATA[1], 13)    //(DISPDATA[0]&= ~(1<<4))
//#define SET_VALUE_2B                      LCD_RST_BIT(DISPDATA[1], 13)    //(DISPDATA[0]|= (1<<3))
//#define RESET_VALUE_2B                    LCD_RST_BIT(DISPDATA[1], 13)    //(DISPDATA[0]&= ~(1<<3))
//#define SET_VALUE_3G                      LCD_RST_BIT(DISPDATA[1], 13)   //(DISPDATA[0]|= (1<<2))
//#define RESET_VALUE_3G                    LCD_RST_BIT(DISPDATA[1], 13)    //(DISPDATA[0]&= ~(1<<2))
//#define SET_VALUE_3B                      LCD_RST_BIT(DISPDATA[1], 13)    //(DISPDATA[0]|= (1<<1))
//#define RESET_VALUE_3B                    LCD_RST_BIT(DISPDATA[1], 13)    //(DISPDATA[0]&= ~(1<<1))
//#define SET_VALUE_4G                      LCD_RST_BIT(DISPDATA[1], 13)    //(DISPDATA[0]|= (1<<0))
//#define RESET_VALUE_4G                    LCD_RST_BIT(DISPDATA[1], 13)    //(DISPDATA[0]&= ~(1<<0))
//#define SET_VALUE_4B                      LCD_RST_BIT(DISPDATA[1], 13)    //(DISPDATA[4]|= (1<<9))
//#define RESET_VALUE_4B                    LCD_RST_BIT(DISPDATA[1], 13)    //(DISPDATA[4]&= ~(1<<9))
//#define SET_VALUE_T1                      (DISPDATA[4] |= 0x00000100)    //(DISPDATA[4]|= (1<<8))
//#define RESET_VALUE_T1                    (DISPDATA[4] &= 0xfffffeff)    //(DISPDATA[4]&= ~(1<<8))
    #define SET_VALUE_W3                      LCD_SET_BIT(DISPDATA[1], 27)    //(DISPDATA[4]|= (1<<11))
    #define RESET_VALUE_W3                    LCD_RST_BIT(DISPDATA[1], 27)    //(DISPDATA[4]&= ~(1<<11))
//#define SET_VALUE_L3                      (DISPDATA[4] |= 0x00000400)    //(DISPDATA[4]|= (1<<10))
//#define RESET_VALUE_L3                    (DISPDATA[4] &= 0xfffffbff)    //(DISPDATA[4]&= ~(1<<10))
    #define SET_VALUE_5E                      LCD_SET_BIT(DISPDATA[1], 9)    //(DISPDATA[0]|= (1<<7))
    #define RESET_VALUE_5E                    LCD_RST_BIT(DISPDATA[1], 9)   //(DISPDATA[0]&= ~(1<<7))

    #define SET_VALUE_5C                      LCD_SET_BIT(DISPDATA[1], 8)   //(DISPDATA[0]|= (1<<8))
    #define RESET_VALUE_5C                    LCD_RST_BIT(DISPDATA[1], 8)   //(DISPDATA[0]&= ~(1<<8))

    #define SET_VALUE_6E                      LCD_SET_BIT(DISPDATA[1], 7)    //(DISPDATA[0]|= (1<<9))
    #define RESET_VALUE_6E                    LCD_RST_BIT(DISPDATA[1], 7)    //(DISPDATA[0]&= ~(1<<9))

    #define SET_VALUE_6C                      LCD_SET_BIT(DISPDATA[1], 6)    //(DISPDATA[0]|= (1<<10))
    #define RESET_VALUE_6C                    LCD_RST_BIT(DISPDATA[1], 6)    //(DISPDATA[0]&= ~(1<<10))

    #define SET_VALUE_7E                      LCD_SET_BIT(DISPDATA[1], 5)   //(DISPDATA[0]|= (1<<11))
    #define RESET_VALUE_7E                    LCD_RST_BIT(DISPDATA[1], 5)    //(DISPDATA[0]&= ~(1<<11))

    #define SET_VALUE_7C                      LCD_SET_BIT(DISPDATA[1], 4)    //(DISPDATA[0]|= (1<<12))
    #define RESET_VALUE_7C                    LCD_RST_BIT(DISPDATA[1], 4)    //(DISPDATA[0]&= ~(1<<12))

//#define SET_VALUE_S2                      LCD_SET_BIT(DISPDATA[1], 9)    //(DISPDATA[0]|= (1<<13))
//#define RESET_VALUE_S2                    LCD_SET_BIT(DISPDATA[1], 9)    //(DISPDATA[0]&= ~(1<<13))

//#define SET_VALUE_S7                      LCD_SET_BIT(DISPDATA[1], 9)    //(DISPDATA[0]|= (1<<14))
//#define RESET_VALUE_S7                    LCD_SET_BIT(DISPDATA[1], 9)    //(DISPDATA[0]&= ~(1<<14))

    #define SET_VALUE_8E                      LCD_SET_BIT(DISPDATA[1], 1)    //(DISPDATA[0]|= (1<<15))
    #define RESET_VALUE_8E                    LCD_RST_BIT(DISPDATA[1], 1)    //(DISPDATA[0]&= ~(1<<15))

    #define SET_VALUE_8C                      LCD_SET_BIT(DISPDATA[1], 0)    //(DISPDATA[0]|= (1<<16))
    #define RESET_VALUE_8C                    LCD_RST_BIT(DISPDATA[1], 0)    //(DISPDATA[0]&= ~(1<<16))

    #define SET_VALUE_9E                      DISPDATA[1] |= 0x80000000    //(DISPDATA[0]|= (1<<17))
    #define RESET_VALUE_9E                    DISPDATA[1] &= 0x7fffFfff    //(DISPDATA[0]&= ~(1<<17))

    #define SET_VALUE_9C                      DISPDATA[1] |= 0x40000000    //(DISPDATA[0]|= (1<<18))
    #define RESET_VALUE_9C                    DISPDATA[1] &= 0xBfffFfff    //(DISPDATA[0]&= ~(1<<18))

    #define SET_VALUE_10E                     LCD_SET_BIT(DISPDATA[1], 29)    //(DISPDATA[0]|= (1<<19))
    #define RESET_VALUE_10E                   LCD_RST_BIT(DISPDATA[1], 29)    //(DISPDATA[0]&= ~(1<<19))

    #define SET_VALUE_10C                     LCD_SET_BIT(DISPDATA[1], 28)    //(DISPDATA[0]|= (1<<26))
    #define RESET_VALUE_10C                   LCD_RST_BIT(DISPDATA[1], 28)    //(DISPDATA[0]&= ~(1<<26))

    #define SET_VALUE_1F                      LCD_SET_BIT(DISPDATA[0], 14)    //(DISPDATA[1]|= (1<<6))
    #define RESET_VALUE_1F                    LCD_RST_BIT(DISPDATA[0], 14)    //(DISPDATA[1]&= ~(1<<6))

    #define SET_VALUE_1A                      LCD_SET_BIT(DISPDATA[0], 13)    //(DISPDATA[1]|= (1<<5))
    #define RESET_VALUE_1A                    LCD_RST_BIT(DISPDATA[0], 13)    //(DISPDATA[1]&= ~(1<<5))

//#define SET_VALUE_2F                      (DISPDATA[1] |= 0x00000010)    //(DISPDATA[1]|= (1<<4))
//#define RESET_VALUE_2F                    (DISPDATA[1] &= 0xffffffef)    //(DISPDATA[1]&= ~(1<<4))
//#define SET_VALUE_2A                      (DISPDATA[1] |= 0x00000008)    //(DISPDATA[1]|= (1<<3))
//#define RESET_VALUE_2A                    (DISPDATA[1] &= 0xfffffff7)    //(DISPDATA[1]&= ~(1<<3))
//#define SET_VALUE_3F                      (DISPDATA[1] |= 0x00000004)    //(DISPDATA[1]|= (1<<2))
//#define RESET_VALUE_3F                    (DISPDATA[1] &= 0xfffffffb)    //(DISPDATA[1]&= ~(1<<2))
//#define SET_VALUE_3A                      (DISPDATA[1] |= 0x00000002)    //(DISPDATA[1]|= (1<<1))
//#define RESET_VALUE_3A                    (DISPDATA[1] &= 0xfffffffd)    //(DISPDATA[1]&= ~(1<<1))
//#define SET_VALUE_4F                      (DISPDATA[1] |= 0x00000001)    //(DISPDATA[1]|= (1<<0))
//#define RESET_VALUE_4F                    (DISPDATA[1] &= 0xfffffffe)    //(DISPDATA[1]&= ~(1<<0))
//#define SET_VALUE_4A                      (DISPDATA[4] |= 0x00200000)    //(DISPDATA[4]|= (1<<21))
//#define RESET_VALUE_4A                    (DISPDATA[4] &= 0xffdfffff)    //(DISPDATA[4]&= ~(1<<21))

#define SET_VALUE_W1                      LCD_SET_BIT(DISPDATA[0], 26)    //(DISPDATA[4]|= (1<<20))
#define RESET_VALUE_W1                    LCD_RST_BIT(DISPDATA[0], 26)    //(DISPDATA[4]&= ~(1<<20))

#define SET_VALUE_W2                      LCD_SET_BIT(DISPDATA[0], 27)    //(DISPDATA[4]|= (1<<23))
#define RESET_VALUE_W2                    LCD_RST_BIT(DISPDATA[0], 27)    //(DISPDATA[4]&= ~(1<<23))

//#define SET_VALUE_L4                      (DISPDATA[4] |= 0x00400000)    //(DISPDATA[4]|= (1<<22))
//#define RESET_VALUE_L4                    (DISPDATA[4] &= 0xffbfffff)    //(DISPDATA[4]&= ~(1<<22))

    #define SET_VALUE_5D                     LCD_SET_BIT(DISPDATA[0], 9)    //(DISPDATA[1]|= (1<<7))
    #define RESET_VALUE_5D                   LCD_RST_BIT(DISPDATA[0], 9)    //(DISPDATA[1]&= ~(1<<7))

//#define SET_VALUE_DP5                     (DISPDATA[1] |= 0x00000100)    //(DISPDATA[1]|= (1<<8))
//#define RESET_VALUE_DP5                   (DISPDATA[1] &= 0xfffffeff)    //(DISPDATA[1]&= ~(1<<8))

    #define SET_VALUE_6D                      LCD_SET_BIT(DISPDATA[0], 7)    //(DISPDATA[1]|= (1<<9))
    #define RESET_VALUE_6D                    LCD_RST_BIT(DISPDATA[0], 7)    //(DISPDATA[1]&= ~(1<<9))

//#define SET_VALUE_DP6                     (DISPDATA[1] |= 0x00000400)    //(DISPDATA[1]|= (1<<10))
//#define RESET_VALUE_DP6                   (DISPDATA[1] &= 0xfffffbff)    //(DISPDATA[1]&= ~(1<<10))

    #define SET_VALUE_7D                      LCD_SET_BIT(DISPDATA[0], 5)    //(DISPDATA[1]|= (1<<11))
    #define RESET_VALUE_7D                    LCD_RST_BIT(DISPDATA[0], 5)    //(DISPDATA[1]&= ~(1<<11))

//#define SET_VALUE_DP7                     (DISPDATA[1] |= 0x00001000)    //(DISPDATA[1]|= (1<<12))
//#define RESET_VALUE_DP7                   (DISPDATA[1] &= 0xffffefff)    //(DISPDATA[1]&= ~(1<<12))
//#define SET_VALUE_S1                      (DISPDATA[1] |= 0x00002000)    //(DISPDATA[1]|= (1<<13))
//#define RESET_VALUE_S1                    (DISPDATA[1] &= 0xffffdfff)    //(DISPDATA[1]&= ~(1<<13))
//#define SET_VALUE_S8                      (DISPDATA[1] |= 0x00004000)    //(DISPDATA[1]|= (1<<14))
//#define RESET_VALUE_S8                    (DISPDATA[1] &= 0xffffbfff)    //(DISPDATA[1]&= ~(1<<14))

    #define SET_VALUE_8D                      LCD_SET_BIT(DISPDATA[0], 1)    //(DISPDATA[1]|= (1<<15))
    #define RESET_VALUE_8D                    LCD_RST_BIT(DISPDATA[0], 1)    //(DISPDATA[1]&= ~(1<<15))

//#define SET_VALUE_DP8                     (DISPDATA[1] |= 0x00010000)    //(DISPDATA[1]|= (1<<16))
//#define RESET_VALUE_DP8                   (DISPDATA[1] &= 0xfffeffff)    //(DISPDATA[1]&= ~(1<<16))

    #define SET_VALUE_9D                      DISPDATA[0] |= 0x80000000    //(DISPDATA[1]|= (1<<17))
    #define RESET_VALUE_9D                    DISPDATA[0] &= 0x7fffFfff    //(DISPDATA[1]&= ~(1<<17))

    #define SET_VALUE_DP9                     DISPDATA[0] |= 0x40000000    //(DISPDATA[1]|= (1<<18))
    #define RESET_VALUE_DP9                   DISPDATA[0] &= 0xBfffFfff    //(DISPDATA[1]&= ~(1<<18))

    #define SET_VALUE_10D                     LCD_SET_BIT(DISPDATA[0], 29)    //(DISPDATA[1]|= (1<<19))
    #define RESET_VALUE_10D                   LCD_RST_BIT(DISPDATA[0], 29)    //(DISPDATA[1]&= ~(1<<19))

#define SET_VALUE_S9                      LCD_SET_BIT(DISPDATA[0], 28)    //(DISPDATA[1]|= (1<<26))
#define RESET_VALUE_S9                    LCD_RST_BIT(DISPDATA[0], 28)    //(DISPDATA[1]&= ~(1<<26))
#endif
