#ifndef __GLOBAL_H__
#define __GLOBAL_H__

#include "bsp.h"

#define	uint08 uint8_t
#define	uint16 uint16_t
#define uint32 uint32_t
#define	int08 int8_t		
#define	int16 int16_t
#define	int32 int32_t
#define u8    uint8_t
#define u16   uint16_t
#define u32   uint32_t
#define uchar   uint8_t

#endif

