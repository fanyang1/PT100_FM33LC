#ifndef __BSP_H__
#define __BSP_H__

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdarg.h>

#include "fm33lc0xx_ll_rcc.h"
#include "fm33lc0xx_ll_lptim.h"
#include "fm33lc0xx_ll_lpuart.h"
#include "fm33lc0xx_ll_gpio.h"
#include "fm33lc0xx_ll_uart.h"
#include "fm33lc0xx_ll_vref.h"
#include "fm33lc0xx_ll_iwdt.h"
#include "fm33lc0xx_ll_wwdt.h"
#include "fm33lc0xx_ll_pmu.h"
#include "fm33lc0xx_ll_flash.h"
#include "fm33lc0xx_ll_svd.h"
#include "fm33lc0xx_ll_aes.h"
#include "fm33lc0xx_ll_rmu.h"
#include "fm33lc0xx_ll_rng.h"
#include "fm33lc0xx_ll_opa.h"
#include "fm33lc0xx_ll_comp.h"
#include "fm33lc0xx_ll_hdiv.h"
#include "fm33lc0xx_ll_i2c.h"
#include "fm33lc0xx_ll_spi.h"
#include "fm33lc0xx_ll_u7816.h"
#include "fm33lc0xx_ll_bstim.h"
#include "fm33lc0xx_ll_gptim.h"
#include "fm33lc0xx_ll_atim.h"
#include "fm33lc0xx_ll_crc.h"
#include "fm33lc0xx_ll_dma.h"
#include "fm33lc0xx_ll_rtc.h"
#include "fm33lc0xx_ll_lcd.h"
#include "mf_config.h"



#include "main.h"

//#include "bintohex.h"
//#include "FM33G0XX.h"
#include "global.h"   //芯片全局变量定义
#include "user_init.h"    //芯片初始化

//#include "user_define.h"  //用户宏定义_自行添加文件
#include "Lcdzhen.h"      //LCD每个段码宏定义


#include "bsp_gpio.h"
//#include "bsp_flash.h"
#include "bsp_sleep.h"
//#include "bsp_lptimer.h"
#include "bsp_uart.h"
//#include "bsp_lpuart.h"
#include "bsp_lcd.h"
#include "bsp_adc.h"
#include "bsp_rtc.h"
//#include "bsp_trng.h"
#include "bsp_debug.h"
#include "bsp_user_lib.h"
//#include "bsp_beep.h"
//#include "bsp_protocol.h"
//#include "bsp_svd.h"
#include "bsp_i2c.h"
#include "pt100.h"
#include "mcp3425.h"
#include "bsp_btim.h"

#include "usb.h"
#include "stream.h"
#include "cdc_vcp.h"

#include "usb_bsp.h"
#include "usb_dcd_int.h"
#include "usbd_desc.h"
#include "usbd_usr.h"
#include "cdc_core.h"

//#include "FM175XX_REG.h"
//#include "READER_API.h"
//#include "LPCD_API.h"
//#include "LPCD_CFG.h"

void bsp_Init(void);



#endif
