#ifndef __BSP_LCD_H__
#define __BSP_LCD_H__

#include "bsp.h"
#include <stdbool.h>

extern uint32_t DISPDATA[10]; //定义数据缓冲区
extern uint32_t DispBuf[10];    //定义数据寄存器/* 为了优化显示逻辑部分，引入刷屏标志。0 表示显示函数只改写缓冲区，不写屏。1 表示直接写屏（同时写缓冲区） */
extern uint8_t s_ucUpdateEn;
extern uint32 Count32;
 
void LCD_ShowALL(void);
void LCD_ShowNO(void);
void LCD_StartDraw(void);
void LCD_EndDraw(void);
void LCD_BufToPanel(void);
void LCD_IO_Init(void);
void bsp_LCDinit(void);
void LCD_COMSEGEN_Init(void);
void Init_LCD(void);
uint32_t LCD_DF_Calc(uint32_t LMUX, uint32_t WFT, uint32_t FreqWanted);
unsigned char  LCD_Table( unsigned char  Show );
void LCD_Num_Data( uint08 Num, uint08 Show);
void LCD_Show_Count(uint32 ShowData);
void LCD_Show_Time(void);



void LcdInit(void);
void LcdDisplayAll(bool state);
void LcdDisplayBIT(uint32_t x, uint32_t y, bool value);
void LcdDisplayNumber(uint32_t number);

#endif
