#ifndef __BSP_NB_H 
#define __BSP_NB_H

#include "bsp.h"

//typedef struct 
//{
//	u8  ServerIP[4];        //IP 地址
//	u16 UDPPort;            //UDP端口号
//	u16 TCPPort;            //TCP端口号
//	u8  YuORIP;             //表示IP地址 还是域名方式	
//	u8  ChongQiMess[2];     //模块重启信息
//	u16 HeartTime;          //设置心跳时间间隔
//	u16 OpenTime;           //模块唤醒时间
//	u16 HuiChuanTime;       //设置回传时间
//	u16 HuiChuanNum;        //设置回传次数
//	u16 ZhuiZongTime;       //设置追踪时间
//	u8 ShangChuanTimeH;     //上传的具体时间点 时
//	u8 ShangChuanTimeM;     //上传的具体时间点 分
//	u16 ShangChuanCnt;      //模块每天上传次数
//	u8  lightSwitchOff;		//光敏开、关
//	u8  moveSwitchOff;		//移动开、关
//	//u8  alarmSwitch[2];		//终端控制开关  光敏  移动  
//	u16 connectFailedNum;	//服务器连接失败次数100次
//	u8  backUpServerIP[4];  //连接主服务器失败以后，备份IP 地址
//	u16 backUpTCPPort;		//连接主服务器失败以后，备份TCP端口号
//	u8  eepromFlag;			//EEPROM是否被写入标记
//	u16 loginNum;           //登录客户服务器次数，默认100次，9999次表示永久登录客户服务器，不在切回公司服务器
//	u16 loginCnt;			//登录客户服务器次数计数
//	//以上参数需要保存 40个字节
//	
//	u8  alarm[15];          //模块报警状态   报警 正常 欠压等 
//	u8  DianLiang[3];       //模块电量 99.9%
//	u8  IMEI[15];           //模块IMEI号码
//	u8  CSQ[2];             //模块信号
//	u8  Status;             //模块工作状态   休眠 运行 等待
//	u16 serNum;             //报文序列号
//	u8  StationAdd[32];     //基站位置数据
//  u8  PSM;                //设备处于PSM状态
//	u8  YuMing[25];         //域名

//  u8  At_ErrCnt;    //AT指令重发次数
//	u16 At_WaitTime;
//	u8  At_TimeOut;
//	
//  uint32_t PSM_Time;      //psm时间
//	u8  CCID[20];           //读取SIM卡唯一标识号
//	u8  NO_SIM;             //判断模块是否有SIM卡
//  u8  Model;               //模块型号

//	u16 ShangChuanDelay;    //模块每天上传间隔时间 一天3次即 8小时间隔  
//	u16 QieHuanCnt;         //模块失败次数达到多少切换服务器
//	u16 GPSTime;            //每天上报前30分钟校时一次
//}t_nb; 

typedef struct 
{
	u16 Delay_time;

	u8  IMEI[15];           //模块IMEI号码
	u8  CSQ[2];             //模块信号
	u8  NetStatus;      //模块工作状态   休眠 运行 等待
    u8  PSM;                //设备处于PSM状态\

	u32 Flash_Address;
	
  u8  DeviceID[7];
	u8 OneNet_ID; 
	
	u8  year;
	u8  month;
	u8  day;
	u8  hour;
	u8  min;
	u8  sec;
	
	u8  Flow[4];
	u8  Pressure[4];
	
	u16  send_cnt;
	u16  voltage;
	u8   rssi;
	u8  Version[4]; //NB模块内部固件版本号
	
	u8  buffered;
	
	u8  Reserved[16];
	
  u8  At_ErrCnt;    //AT指令重发次数
	u8  Recieve_Err;
	
	u16 At_WaitTime;
	u8  At_TimeOut;
	u8  ControlFlag;
	
  uint32_t PSM_Time;      //psm时间
	u8  CCID[20];           //读取SIM卡唯一标识号
	u8  NO_SIM;             //判断模块是否有SIM卡
  u8  Model;              //模块型号

	u8 OnlineTime[6];       //年月日时分秒
	u16 OnlineDelay;    //模块每天上传间隔时间 一天3次即 8小时间隔  
	u16 OnlineCount;
	
	u8 SpecificTime[6];
  u16 TimeWake;
	
	u8 Init_Suatus;  //上电后的状态
}t_nb;
extern t_nb Module;

#define  BC30      1
#define  BC35      2
#define  BC95      3
#define  TPB21_5   4
#define  TPB23     5
#define  M5311     6
#define  M5312     7
#define  M5313     8

#define  ERR_MAX   10


void bsp_NbSaveParam(void);
void bsp_NbLoadParam(void);

void NB_SendString(uint08* buf);

void Nb_MainTask(void);
void Nb_Check_Time(void);
void NB_Init(void);

#endif

