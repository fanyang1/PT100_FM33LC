#ifndef __BSP_PROTOCOL_H
#define __BSP_PROTOCOL_H

#include "bsp.h"

#define NB_MAX_LEN      15
#define RS232_MAX_LEN   3
#define RS485_MAX_LEN   3

#define RBUF_MAX_LEN  60

extern unsigned char isEmpty(UART_Type* UARTx);
extern unsigned char buffSize(UART_Type* UARTx);
extern unsigned char isFull(UART_Type* UARTx);
extern void pushBuff (UART_Type* UARTx,u8 data);                                        //定义一个串口一接收数据的缓冲区
extern u8 popBuff(UART_Type* UARTx);
extern void clearBuff (UART_Type* UARTx);
	
typedef struct 
{      
    unsigned char rbuf[RBUF_MAX_LEN];
    unsigned short len;
}DataBuf;

typedef struct uartReceive                                                     
{			 			
    //unsigned char buff[rebufLen];		                                
    short readIndex;                                                
    short writeIndex;					 	       
}uartRec;

extern DataBuf Nb_Buf[NB_MAX_LEN];
extern DataBuf Rs232_Buf[RS232_MAX_LEN];
extern DataBuf Rs485_Buf[RS485_MAX_LEN];

extern DataBuf NbTemp;
extern DataBuf Rs232Temp;
extern DataBuf Rs485Temp;

extern uartRec NbReceive;
extern uartRec Rs232Receive;
extern uartRec Rs485Receive;






#endif


