#ifndef __BSP_SPI_H
#define __BSP_SPI_H

#include "bsp.h"  


uint8_t spi2_send_rw(unsigned char address);
void bsp_spi_init(void);
uint32_t SpiWriteAndRead(uint32_t data);
void SpiWriteData(uint8_t data);
uint8_t SpiReadData(uint8_t data);

#endif
