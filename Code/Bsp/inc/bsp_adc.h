#ifndef __BSP_ADC_H
#define __BSP_ADC_H		
 
#include "bsp.h" 
 
void bsp_adcInit(void);
uint32_t GetVref1p22Sample(void);
uint32_t GetVoltageSample(unsigned int adc_ch);
uint32_t GetVoltage(unsigned int adc_ch);
uint16_t Get_Vbat(void);
void show_bat(void);

						 
#endif 
